<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login MMC Kotawaringin Barat</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/template_login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/template_login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/template_login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/template_login/css/main.css">
<!--===============================================================================================-->
<link rel="icon" href="<?php echo base_url(); ?>assets/template_login/images/favicon.png" type="image/x-icon">
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url(); ?>assets/template_login/images/bundaran-pancasila-kobar.jpg');">
			<div class="wrap-login100">
				<?php echo form_open('','class="login100-form validate-form"'); ?>
					<span class="login100-form-logo">
					<img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->logo; ?>" width="150" class="img-responsive" alt="">
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

                    
                    <?php show_input_error(); ?>
                    <?php show_message(); ?>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100 control-input" type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100 control-input" type="password" name="pass" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>