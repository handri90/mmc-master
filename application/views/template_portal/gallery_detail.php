<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner categoty-style-1">
                                <!-- post body -->
								<div class="post-head">
                                    <h2 class="title"><strong>Foto</strong></h2>
                                </div>
                                <div class="post-body">
                                    <div class="row row-m">
                                        <div class="ug-gallery-wrapper ug-under-780 ug-theme-tilesfixed" style="max-width: 100%; min-width: 150px; overflow: visible; height: 171px; width: auto;">
											<div class="ug-thumbs-grid" style="width: 600px; height: 171px; position: absolute; margin: 0px; left: 38px; top: 0px">
												<div class="ug-thumbs-grid-inner" style="width: 600px;height: 171px;">
                                                <?php
                                                $i = 1;
                                                $j = 10;
                                                foreach ($foto as $key=>$val) {
                                                ?>
                                                <div class="ug-thumb-wrapper ug-tile ug-tile-clickable ug-thumb-ratio-set" style="z-index: <?php echo $i; ?>;opacity: 1;background-color: rgb(240, 240, 240);border-width: 3px;border-style: solid;border-color: rgb(240, 240, 240);border-radius: 2px;box-shadow: rgb(139, 139, 139) 1px 1px 3px 2px;width: 180px;height: 151px;position: absolute;margin: 0px;left: <?php echo $j.'px'; ?>;top: 10px;">
                                                <img class="ug-thumb-image ug-trans-enabled showgal" style="max-width: none !important;max-height: none !important;display: block;border: none;padding: 0px !important;margin: 0px !important;position: absolute;opacity: 1;width: 218px;height: 145px;left: -22px;top: 0px;" alt="<?php echo $val->caption; ?>"
                                                    src="<?php echo base_url().$this->config->item("gallery_path")."/".$val->filename; ?>"
                                                    onClick="showGalKeg(<?php echo $i; ?>)">
                                                </div>
                                                <?php
                                                $i++;
                                                    $j+=200;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>
                    
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <span class="leftArrow">&lt;</span>
  <span class="rightArrow">&gt;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>