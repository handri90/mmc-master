<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <section class="slider-inner">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="col-xs-12 col-sm-8 col-md-8 thm-padding">
                            <div class="slider-wrapper">
                                <div id="owl-slider" class="owl-carousel owl-theme">
                                    <!-- Slider item one -->
                                    <?php
                                    foreach($list_berita_slide_per_kategori as $key=>$val){
                                      ?>
                                      <div class="item">
                                        <div class="slider-post post-height-1">
                                            <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="news-image"><img src="<?php echo base_url().$this->config->item("berita_path")."/".$val->filename; ?>" alt="" class="img-responsive"></a>
                                            <div class="post-text">
                                                <span class="post-category"><?php echo $val->nama_kategori_berita; ?></span>
                                                <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li class="date"><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
<!--                                                     <li class="view">Dibaca <?php echo $val->read_count; ?> kali</li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                      <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 thm-padding">
                            <div class="row slider-right-post thm-margin">
                                <?php
                                foreach($list_berita_acak_by_kategori as $key=>$val){
                                  ?>
                                  <div class="col-xs-6 col-sm-12 col-md-12 thm-padding">
                                      <div class="slider-post post-height-2">
                                          <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="news-image"><img src="<?php echo base_url().$this->config->item('berita_path')."/".$val->filename; ?>" alt="" class="img-responsive"></a>
                                          <div class="post-text">
                                              <span class="post-category"><?php echo $val->nama_kategori_berita; ?></span>
                                              <h4><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>"><?php echo $val->judul; ?></a></h4>
                                              <ul class="authar-info">
                                                  <li class="hidden-xs"><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
<!--                                                   <li class="view hidden-xs hidden-sm">Dibaca <?php echo $val->read_count; ?> kali</li> -->
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                                  <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!-- post body -->
                                <div class="post-head">
                                    <h2 class="title"><strong><?php echo $kategori->nama_kategori_berita; ?></strong></h2>
                                </div>
                                <div class="post-body">
                                        <?php
                                        if(!empty($berita)){
                                        foreach($berita as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list">
                                            <div class="img-wrapper">
                                                <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="thumb"><img src="<?php echo base_url().$this->config->item("berita_path")."/".$val->filename; ?>" alt="" class="img-responsive"></a>
                                            </div>
                                            <div class="post-info-2">
                                                <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="title"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li><i class="ti-timer"></i><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                  }else{
                                    echo 'Belum ada data';
                                  }
                                        ?>
                                </div>
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>