<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!-- post body -->
                                <div class="post-head">
                                    <h2 class="title"><strong>Album</strong></h2>
                                </div>
                                <div class="post-body">
                                        <?php
                                        if(!empty($album)){
                                        foreach($album as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list">
                                            <div class="img-wrapper">
                                                <img onClick="functShowPop(this)" src="<?php echo base_url().$this->config->item("gallery_path")."/".$val->filename; ?>" alt="<?php echo $val->nama_gallery; ?>" class="img-responsive">
                                            </div>
                                            <div class="post-info-2">
                                                <h2><a id="nih" onClick="functShowPop(this)" src="<?php echo base_url().$this->config->item("gallery_path")."/".$val->filename; ?>" alt="<?php echo $val->nama_gallery; ?>" class="title"><?php echo $val->nama_gallery; ?></a></h2>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                  }else{
                                    echo 'Belum ada data';
                                  }
                                        ?>
                                </div>
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <?php if (isset($links)) { ?>
                                                <?php echo $links ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>
                    
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
  
  <script>
// Get the modal
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
function functShowPop(a){
	modal.style.display = "block";
    modalImg.src = a.getAttribute("src");
    captionText.innerHTML = a.getAttribute("alt");
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>