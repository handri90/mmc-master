<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126622925-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126622925-1');
</script>

<title><?php echo $title; ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:site_name" content="mmckotawaringinbarat" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge, chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/ >
<meta name="language" content="en" />
<meta name="description" content="<?php echo $description; ?>" />
<meta name="author" content="MMC Kobar" />
<meta name="keywords" content="<?php echo $keyword; ?>">
                
                <meta name="googlebot-news" content="index,follow" />
                <meta name="googlebot" content="index,follow" />

                <meta name="robots" content="index, follow" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="google-site-verification" content="MbJy6ovMlrBeXXBRJouefKHIWcKrYZ60GsorQqiRkr0" />

<meta property="og:url"           content="<?php echo base_url(); ?>" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="<?php echo $title; ?>" />
  <meta property="og:description"   content="<?php echo $description; ?>" />
  <meta property="og:image"         content="<?php echo $image; ?>" />



<!-- jquery ui css -->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!--Animate css-->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <!-- Navigation css-->
        <link href="<?php echo base_url(); ?>assets/template_portal/bootsnav/css/bootsnav.css" rel="stylesheet" type="text/css"/>
        <!-- youtube css -->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/RYPP.css" rel="stylesheet" type="text/css"/>
        <!-- font awesome -->
        <link href="<?php echo base_url(); ?>assets/template_portal/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- themify-icons -->
        <link href="<?php echo base_url(); ?>assets/template_portal/themify-icons/themify-icons.css" rel="stylesheet" type="text/css"/>
        <!-- weather-icons -->
        <link href="<?php echo base_url(); ?>assets/template_portal/weather-icons/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>
        <!-- flat icon -->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/flaticon.css" rel="stylesheet" type="text/css"/>
        <!-- Important Owl stylesheet -->
        <link href="<?php echo base_url(); ?>assets/template_portal/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
        <!-- Default Theme -->
        <link href="<?php echo base_url(); ?>assets/template_portal/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
        <!-- owl transitions -->
        <link href="<?php echo base_url(); ?>assets/template_portal/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
        <!-- style css -->
        <link href="<?php echo base_url(); ?>assets/template_portal/css/style.css" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='<?php echo base_url(); ?>assets/template_portal/css/unite-gallery.css' type='text/css' />

        <link rel="icon" href="<?php echo base_url(); ?>assets/template_portal/images/favicon.png" type="image/x-icon">
</head>
<body>
<!-- PAGE LOADER -->
        <!-- div class="se-pre-con"></div -->
        <!-- *** START PAGE HEADER SECTION *** -->
        <header>
            <!-- START HEADER TOP SECTION -->
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <!-- Start header social -->
                            <div class="header-social">
                                <ul>
                                    <?php
                                    $class1 = "";
                                    $class2 = "";
                                    $link = "";
                                    if(isset($list_media_sosial)){
                                        foreach($list_media_sosial as $key){
                                            if($key->link_medsos == ""){
                                                continue;
                                            }
                                            if($key->nama_medsos == 'facebook'){
                                                $class2 = "fa-facebook";
                                            }elseif($key->nama_medsos == 'youtube'){
                                                $class2 = "fa-youtube-play";
                                            }elseif($key->nama_medsos == 'twitter'){
                                                $class2 = "fa-twitter";
                                            }elseif($key->nama_medsos == 'instagram'){
                                                $class2 = "fa-instagram";
                                            }
                                          ?>
                                          <li>
                                            <a target="_blank" href="<?php echo $key->link_medsos; ?>">
                                                <i class="fa <?php echo $class2; ?>"></i>
                                            </a>
                                          </li>
                                          <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <!-- End of /. header social -->
                            <!-- Start top left menu -->
                            
                            <!-- End of /. top left menu -->
                        </div>
                        <!-- Start header top right menu -->
                        <div class="hidden-xs col-md-6 col-sm-6 col-lg-6">
                            <div class="header-right-menu">
                                <ul>
                                    <li><i class="fa fa-lock"></i><a href="<?php echo base_url(); ?>login"> Login</a></li>
                                </ul>
                            </div>
                        </div> <!-- end of /. header top right menu -->
                    </div> <!-- end of /. row -->
                </div> <!-- end of /. container -->
            </div>
            <!-- END OF /. HEADER TOP SECTION -->
            <!-- START MIDDLE SECTION -->
            <div class="header-mid hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="logo">
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->logo; ?>" width="150" class="img-responsive" alt=""></a>

                            </div>
                        </div>
                        <div class="col-sm-10" style="padding-top: 10px;">
                            <a href="#"><img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->banner_header; ?>" class="img-responsive" alt=""></a>
                    	</div>
                    </div>
                </div>
            </div>
            <!-- END OF /. MIDDLE SECTION -->
            <!-- START NAVIGATION -->
            <nav class="navbar navbar-default navbar-sticky navbar-mobile bootsnav">
                <!-- Start Top Search -->
                <div class="top-search">
                    <div class="container">
                    	<form action="<?php echo base_url(); ?>search" method="post">
                        	<div class="input-group">
                            	<span class="input-group-addon"><i class="fa fa-search"></i></span>
                            	<input type="text" name="search" class="form-control" placeholder="Search">
                            	<span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        	</div>
                    	</form>
                    </div>
                </div>
                <!-- End Top Search -->
                <div class="container">            
                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        </ul>
            		</div>
                    <!-- End Atribute Navigation -->
                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#brand"><img src="assets/template_portal/images/logo.png" class="logo" alt=""></a>
                    </div>
                    <!-- End Header Navigation -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-center" data-in="" data-out="">
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Berita</a>
                                <ul class="dropdown-menu">
                                    <?php
                                    foreach($list_kategori_menu as $key=>$val){
                                      ?>
                                      <li><a href="<?php echo base_url().$val->slug_kategori; ?>"><?php echo $val->nama_kategori_berita; ?></a></li>
                                      <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Pojok Pemilu</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url(); ?>beritapojokpemilu">Berita</a></li>
                                    <!-- li><a href="<?php echo base_url(); ?>pojokpemilu">Info Grafis</a></li -->
                                </ul>
                            </li>
                            <!-- li><a href="#">Komunitas</a></li -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Foto</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url(); ?>kegiatan">Kegiatan</a></li>
                                    <li><a href="<?php echo base_url(); ?>fotokobartempodulu">Kobar Tempo Dulu</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Video</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url(); ?>lintasolahraga">Lintas Olahraga</a></li>
                                    <li><a href="<?php echo base_url(); ?>kontendaerah">Konten Daerah</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>
            <!-- END OF/. NAVIGATION -->
        </header>
        <!-- *** END OF /. PAGE HEADER SECTION *** -->