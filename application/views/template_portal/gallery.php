<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!-- post body -->
                                <div class="post-head">
                                    <h2 class="title"><strong>Album</strong></h2>
                                </div>
                                <div class="post-body">
                                        <?php
                                        foreach($album as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list">
                                            <div class="img-wrapper">
                                                <a href="<?php echo base_url(); ?>foto/<?php echo $val->slug_gallery; ?>"><img src="<?php echo base_url().$this->config->item("gallery_path")."/".$val->filename; ?>" width="345" alt="" class="img-responsive"></a>
                                            </div>
                                            <div class="post-info-2">
                                                <h2><a href="<?php echo base_url(); ?>foto/<?php echo $val->slug_gallery; ?>">
                                                    <?php echo $val->nama_gallery; ?></a></h2>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                        ?>
                                </div>
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <?php if (isset($links)) { ?>
                                                <?php echo $links ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>