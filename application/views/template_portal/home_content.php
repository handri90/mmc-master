        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <!-- START POST BLOCK SECTION -->
            <div class="container">
                <div class="newstricker_inner">
                    <div class="trending"><strong>Trending</strong> Now</div>
                    <div id="NewsTicker" class="owl-carousel owl-theme">
                        <?php
                        $i=0;
                        foreach($list_berita_terpopuler as $key=>$val){
                            ?>
                            <div class="item">
                                <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>"><?php echo $val->judul; ?></a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <section class="slider-inner">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="col-xs-12 col-sm-6 col-md-6 thm-padding">
                            <div class="slider-wrapper">
                                <div id="owl-slider" class="owl-carousel owl-theme">
                                    <?php
                                    foreach($list_berita_terbaru_carousel as $key=>$val){
                                      ?>
                                      <div class="item">
                                        <div class="slider-post post-height-1">
                                            <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="news-image"><img src="<?php echo base_url().$this->config->item('berita_path'); ?>/<?php echo $val->filename; ?>" alt="" class="img-responsive"></a>
                                            <div class="post-text">
                                                <span class="post-category"><?php echo $val->nama_kategori_berita; ?></span>
                                                <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li class="date"><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
<!--                                                     <li class="view">Dibaca <?php echo $val->read_count; ?> kali</li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                      <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 thm-padding">
                            <div class="row slider-right-post thm-margin">
                                <?php
                                foreach($list_berita_acak as $key=>$val){
                                  ?>
                                  <div class="col-xs-6 col-sm-6 col-md-6 thm-padding">
                                      <div class="slider-post post-height-2">
                                          <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="news-image"><img src="<?php echo base_url().$this->config->item('berita_path'); ?>/<?php echo $val->filename; ?>" alt="" class="img-responsive"></a>
                                          <div class="post-text">
                                              <span class="post-category"><?php echo $val->nama_kategori_berita; ?></span>
                                              <h4><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>"><?php echo $val->judul; ?></a></h4>
                                              <ul class="authar-info">
                                                  <li class="hidden-xs"><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
                                                  <li class="view hidden-xs hidden-sm">Dibaca <?php echo $val->read_count; ?> kali</li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                                  <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END OF /. POST BLOCK SECTION -->
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p main-content">
                        <div class="theiaStickySidebar">
                            <!-- START POST CATEGORY STYLE ONE (Popular news) -->
                            <div class="post-inner">
                                    <!--post header-->
                                    <div class="post-head">
                                        <h2 class="title"><strong>Seputar Kobar</strong></h2>
                                    </div>
                                    <!-- post body -->
                                    <div class="post-body">
                                      <?php
                                      foreach($list_berita_seputar_kobar as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list">
                                            <div class="img-wrapper">
                                                <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="thumb"><img src="<?php echo base_url().$this->config->item('berita_path'); ?>/thumbnail/<?php echo $val->filename; ?>" alt="" class="img-responsive"></a>
                                            </div>
                                            <div class="post-info-2">
                                                <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="title"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li><i class="ti-timer"></i><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                      ?>
                                    </div> <!-- /. post body -->
                                    <!--Post footer-->
                                    <div class="post-footer">
                                        <div class="row thm-margin">
                                            <div class="col-xs-12 col-sm-8 col-md-9 thm-padding">
                                                <a href="<?php echo base_url().'seputar-kobar'; ?>" class="more-btn">Berita Lainnya</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.Post footer-->
                                </div>
                            <!-- END OF /. POST CATEGORY STYLE ONE (Popular news) -->
                            <!-- START ADVERTISEMENT -->
                            <div class="add-inner">
                                <img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->banner_content; ?>" class="img-responsive" alt="">
                            </div>
                            <!-- END OF /. ADVERTISEMENT -->
                            <!-- START POST CATEGORY STYLE TWO (Travel news) -->
                            
                            <!--  END OF /. POST CATEGORY STYLE TWO (Travel news) -->
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
            <!-- START FEATURED NEWS -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="featured-inner">
                            <div id="featured-owl" class="owl-carousel">
                              <?php
                              foreach($list_gallery as $key=>$val){
                                ?>
                                <div class="item">
                                    <div class="featured-post">
                                        <a href="#" class="news-image"><img src="<?php echo base_url().$this->config->item('gallery_path')."/".$val->filename; ?>" alt="" class="img-responsive"></a>
                                        <div class="post-text">
                                            <span class="post-category">Foto</span>
                                            <h4><?php echo $val->nama_gallery; ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <?php

                              }
                              ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF /. FEATURED NEWS -->
            <div class="container">
                <div class="row row-m">
                    <div class="col-sm-8 main-content col-p">
                        <div class="theiaStickySidebar">
                            <!-- START POST CATEGORY STYLE THREE (More news) -->
                            <div class="post-inner">
                                    <!--post header-->
                                    <div class="post-head">
                                        <h2 class="title"><strong>Nasional</strong></h2>
                                    </div>
                                    <!-- post body -->
                                    <div class="post-body">
                                        <?php
                                        foreach($list_berita_nasional as $key=>$val){
                                          ?>
                                          <div class="news-list-item articles-list">
                                              <div class="img-wrapper">
                                                  <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="thumb"><img src="<?php echo base_url().$this->config->item('berita_path'); ?>/<?php echo $val->filename; ?>" alt="" class="img-responsive"></a>
                                              </div>
                                              <div class="post-info-2">
                                                  <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="title"><?php echo $val->judul; ?></a></h2>
                                                  <ul class="authar-info">
                                                      <li><i class="ti-timer"></i><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
                                                  </ul>
                                              </div>
                                          </div>
                                          <?php
                                        }
                                        ?>
                                    </div> <!-- /. post body -->
                                    <!--Post footer-->
                                    <div class="post-footer">
                                        <div class="row thm-margin">
                                            <div class="col-xs-12 col-sm-8 col-md-9 thm-padding">
                                                <a href="<?php echo base_url(); ?>nasional" class="more-btn">Berita Lainnya</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.Post footer-->
                                </div>
                            <!-- END OF /. POST CATEGORY STYLE THREE (More news) -->
                            <!-- START ADVERTISEMENT -->
                            <div class="add-inner">
                                <img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->banner_content; ?>" class="img-responsive" alt="">
                            </div>
                            <!-- END OF /. ADVERTISEMENT -->
                        </div>
                    </div>
                    <div class="col-sm-4 rightSidebar col-p">
                        <div class="theiaStickySidebar">
                            <!-- START CATEGORIES WIDGET -->
                            <!-- div class="panel_inner categories-widget">
                                <div class="panel_header">
                                    <h4>Categories</h4>
                                </div>
                                <div class="panel_body">
                                    <ul class="category-list">
                                        <?php
                                        foreach($listKategoriSidebar as $key){
                                          ?>
                                          <li><a href="<?php echo base_url().$key['slugKategori']; ?>"><?php echo $key['nama_kategori_berita']; ?><span><?php echo $key['jml']; ?></span></a></li>
                                          <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div -->
                            <!-- END OF /. CATEGORIES WIDGET -->
                            <!-- START ADVERTISEMENT -->
                            <div class="add-inner">
                                <div id="gpr-kominfo-widget-container"></div>
                            </div>
                            <!-- END OF /. ADVERTISEMENT -->
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
        