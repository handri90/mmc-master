<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner categoty-style-1">
                                <!-- post body -->
                                <div class="post-body">
                                    <div class="row row-m">
                                        <div class="error-page" style="text-align: center;">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="<?php echo base_url(); ?>">return to dashboard</a> or try using the search form.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>