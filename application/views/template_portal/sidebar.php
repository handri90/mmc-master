<div class="col-sm-4 col-p rightSidebar">
                        <div class="theiaStickySidebar">
                            <!-- START SOCIAL ICON -->
                            <div class="social-media-inner">
                                <ul class="social-media clearfix">
                                    <?php
                                    $class1 = "";
                                    $class2 = "";
                                    $link = "";
                                    foreach($list_media_sosial as $key){
                                      if($key->link_medsos == ""){
                                        continue;
                                      }
                                      if($key->nama_medsos == 'facebook'){
                                        $class1 = "fb";
                                        $class2 = "fa-facebook";
                                      }elseif($key->nama_medsos == 'youtube'){
                                        $class1 = "you_tube";
                                        $class2 = "fa-youtube-play";
                                      }elseif($key->nama_medsos == 'twitter'){
                                        $class1 = "twitter";
                                        $class2 = "fa-twitter";
                                      }elseif($key->nama_medsos == 'instagram'){
                                        $class1 = "pint";
                                        $class2 = "fa-instagram";
                                      }
                                      ?>
                                      <li>
                                        <a target="_blank" href="<?php echo $key->link_medsos; ?>" class="<?php echo $class1; ?>">
                                            <i class="fa <?php echo $class2; ?>"></i>
                                        </a>
                                      </li>
                                      <?php
                                    }
                                    ?>
                                </ul> <!-- /.social icon -->     
                            </div>
                            <!-- END OF /. SOCIAL ICON -->
                            <!-- START ADVERTISEMENT -->
                            <!-- div class="add-inner">
                                <img src="<?php echo base_url(); ?>images/add320x270-1.jpg" class="img-responsive" alt="">
                            </div -->
                            <!-- END OF /. ADVERTISEMENT -->
                            <!-- START NAV TABS -->
                            <div class="tabs-wrapper">
                                <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><strong>Berita Terpopuler</strong></a></li>
<!--                                     <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><strong>Berita Terbaru</strong></a></li> -->
                                </ul>
                                <!-- Tab panels one --> 
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="home">

                                        <div class="most-viewed">
                                            <ul id="most-today" class="content tabs-content">
                                                <?php
                                                $i = 1;
                                                foreach($list_berita_terpopuler as $key=>$val){
                                                  ?>
                                                    <li><span class="count"><?php echo $i; ?></span><span class="text"><a href="<?php echo base_url().'berita/'.$val->slug_berita; ?>"><?php echo $val->judul; ?></a></span></li>
                                                  <?php
                                                  $i++;
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
<!--                                     <div role="tabpanel" class="tab-pane fade" id="profile">

                                        <div class="most-viewed">
                                            <ul id="most-today" class="content tabs-content">
                                                <?php
                                                $i = 1;
                                                foreach($listBeritaTerbaru as $key){
                                                  ?>
                                                    <li><span class="count"><?php echo $i; ?></span><span class="text"><a href="<?php echo base_url().'berita/'.$key->slug_berita; ?>"><?php echo $key->judul; ?></a></span></li>
                                                  <?php
                                                  $i++;
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- END OF /. NAV TABS -->
                        </div>
                    </div>