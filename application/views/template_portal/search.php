<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!-- post body -->
                                <div class="post-head">
                                    <h2 class="title"><strong>Hasil Pencarian "<?php echo $keyword; ?>"</strong></h2>
                                </div>
                                <div class="post-body">
                                        <?php
                                        if(!empty($berita)){
                                        foreach($berita as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list">
                                            <div class="img-wrapper">
                                                <a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="thumb"><img src="<?php echo base_url().$this->config->item("berita_path")."/".$val->filename; ?>" alt="" class="img-responsive"></a>
                                            </div>
                                            <div class="post-info-2">
                                                <h2><a href="<?php echo base_url(); ?>berita/<?php echo $val->slug_berita; ?>" class="title"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li><i class="ti-timer"></i><?php echo longdate_indo($val->tanggal_publish_show); ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                  }else{
                                    echo 'Belum ada data';
                                  }
                                        ?>
                                </div>
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <?php if (isset($links)) { ?>
                                                <?php echo $links ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>