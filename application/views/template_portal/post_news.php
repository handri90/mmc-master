<main class="page_main_wrapper">
            <!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post_details_inner">
                                <div class="post_details_block details_block2">
                                    <div class="post-header">
                                        <ul class="td-category">
                                            <li><a class="post-category" href="<?php echo base_url().$berita->slug_kategori; ?>"><?php echo $berita->nama_kategori_berita; ?></a></li>
                                        </ul>
                                        <h2><?php echo $berita->judul; ?></h2>
                                        <ul class="authar-info">
                                            <li><a href="#" class="link">penulis <?php echo $berita->nama_lengkap; ?></a></li>
                                            <li><?php echo longdate_indo($berita->tanggal_publish_show); ?></li>
                                            <li class="chmKu"><a href="#" class="link">dibaca <?php echo $berita->read_count; ?> kali</a></li>
                                        </ul>
                                    </div> 
                                    <figure class="social-icon">
                                        <img src="<?php echo base_url().$this->config->item('berita_path')."/".$berita->filename; ?>" class="img-responsive" alt=""/>
                                        <span style="float:left;line-height: 1.3;margin-top: 10px;font-size: 13px;"><?php echo html_entity_decode($berita->caption); ?></span>
                                        <div>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </div>      
                                    </figure>
                                    <?php
              echo html_entity_decode($berita->deskripsi);
              ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>