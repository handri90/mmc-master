<main class="page_main_wrapper">
<!-- START PAGE TITLE --> 
            <div class="page-title">
            </div>
			<?php
			if(!empty($lastVideo)){
			?>
            <section class="slider-inner cldKu" style="margin-bottom: 48px;">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                            <div class="slider-wrapper">
                                <div>
                                    <!-- Slider item one -->
                                      <div class="item">
                                        <div class="slider-post" style="float: none;text-align: center;">
                                            <iframe class="chgKu" allowfullscreen="1" frameborder="0" src="<?php echo $lastVideo->linkYoutube; ?>" width="80%" height="490"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
            }
			?>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!-- post body -->
                                <div class="post-head">
                                    <h2 class="title"><strong>Video</strong></h2>
                                </div>
                                <div class="post-body">
                                        <?php
                                        if(!empty($video)){
                                        foreach($video as $key=>$val){
                                        ?>
                                        <div class="news-list-item articles-list chnKu">
                                            <div class="han-wrapper">
                                                <iframe class="img-responsive chxKu" src="<?php echo $val->link_youtube; ?>" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="post-info-21">
                                                <h2><a href="#" class="title"><?php echo $val->judul; ?></a></h2>
                                                <ul class="authar-info">
                                                    <li><i class="ti-timer"></i><?php echo longdate_indo($val->tanggal_kegiatan); ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                      }
                                  }else{
                                    echo 'Belum ada data';
                                  }
                                        ?>
                                </div>
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <?php if (isset($links)) { ?>
                                                <?php echo $links ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <?php
                    $this->load->view("template_portal/sidebar.php");
                    ?>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>