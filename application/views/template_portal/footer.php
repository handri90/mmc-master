<!-- *** START FOOTER *** -->
        <footer>
            <div class="container">
                <div class="row">
                    <!-- START FOOTER BOX (About) -->
                    <div class="col-sm-6 footer-box">
                        <div class="about-inner">
                            <img src="<?php echo base_url().$this->config->item("logo_banner_path")."/".$logo_banner->logo; ?>" class="img-responsive" alt="" width="150"/>
                            <p>Dinas Komunikasi Informatika Satistik dan Persandian</p>
                            <table><tr><td style="vertical-align: top;"><i class="ti-location-arrow"></i></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Jalan Sutan Syahrir No. 62, Pangkalan Bun, Kotawaringin Barat</td></tr><tr><td style="vertical-align: top;"><i class="ti-mobile"></i></td><td>&nbsp;</td><td>(0532) 6612159</td></tr><tr><td style="vertical-align: top;"><i class="ti-email"></i></td><td>&nbsp;</td><td>kominfo@kotawaringinbaratkab.go.id</td></tr></table>
                        </div>
                    </div>
                    <!--  END OF /. FOOTER BOX (About) -->
                    <!-- START FOOTER BOX (Twitter feeds) -->
                    
                    <!-- END OF /. FOOTER BOX (Twitter feeds) -->
                    <!-- START FOOTER BOX (Category) -->
                    <div class="col-sm-2 footer-box">
                        <h3 class="wiget-title">Kategori</h3>
                        <ul class="menu-services">
                            <?php
                            foreach($list_kategori_footer as $key){
                              ?>
                              <li><a href="<?php echo base_url().$key->slug_kategori; ?>"><?php echo $key->nama_kategori_berita; ?></a></li>
                              <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- END OF /. FOOTER BOX (Category) -->
                    <!-- START FOOTER BOX (Recent Post) -->
                    <div class="col-sm-4 footer-box">
                        <h3 class="wiget-title">Berita Terbaru</h3>
                        <div class="footer-news-grid">
                            <?php
                            $i = 0;
                            foreach($list_berita_terbaru_footer as $key){
                              if($i==3){
                                break;
                              }
                              ?>
                              <div class="news-list-item">
                                <div class="img-wrapper">
                                    <a href="<?php echo base_url().'berita/'.$key->slug_berita; ?>" class="thumb">
                                        <img src="<?php echo base_url().$this->config->item("berita_path")."/".$key->filename; ?>" alt="" class="img-responsive">
                                        <div class="link-icon">
                                            <i class="fa fa-camera"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-info-2">
                                    <h5><a href="<?php echo base_url().'berita/'.$key->slug_berita; ?>" class="title"><?php echo $key->judul; ?></a></h5>
                                    <ul class="authar-info">
                                        <li><i class="ti-timer"></i><?php echo longdate_indo($key->tanggal_publish_show); ?></li>
                                    </ul>
                                </div>
                            </div>
                              <?php
                              $i++;
                            }
                            ?>
                        </div>
                    </div>
                    <!-- END OF /. FOOTER BOX (Recent Post) -->
                </div>
            </div>
        </footer>
        <!-- *** END OF /. FOOTER *** -->
        <!-- *** START SUB FOOTER *** -->
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <div class="copy">2018 Multimedia Center Kotawaringin Barat</div>
                    </div>
                </div>
            </div>
        </div>
<!-- *** END OF /. SUB FOOTER *** -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/jquery.min.js"></script>
        <!-- jquery ui js -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/jquery-ui.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/bootstrap.min.js"></script>
        <!-- Bootsnav js -->
        <script src="<?php echo base_url(); ?>assets/template_portal/bootsnav/js/bootsnav.js"></script>
        <!-- theia sticky sidebar -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/theia-sticky-sidebar.js"></script>
        <!-- youtube js -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/RYPP.js"></script>
        <!-- owl include js plugin -->
        <script src="<?php echo base_url(); ?>assets/template_portal/owl-carousel/owl.carousel.min.js"></script>
        <!-- custom js -->
        <script src="<?php echo base_url(); ?>assets/template_portal/js/custom.js"></script>
        <script src="<?php echo base_url(); ?>assets/template_portal/js/gpr-widget-kominfo.min.js"></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>assets/template_portal/js/unitegallery.min.js'></script>  
<script type='text/javascript' src='<?php echo base_url(); ?>assets/template_portal/js/ug-theme-tilesgrid.js'></script>

<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var currentSlide = 0;
    var lth = $(".showgal").length;
    if(!(lth > 1)){
    	$(".leftArrow").css("display","none");
        $(".rightArrow").css("display","none");
    }
    $(".showgal").click(function(){
    	$("#myModal").css("display","block");
        $("#img01").attr("src",$(this).attr("src"));
        $("#caption").html($(this).attr("alt"));
        currentSlide = $(".showgal").index(this);
    });
    
    $(".close:eq(0)").click(function(){
    	$("#myModal").css("display","none");
    });
    
    $(".rightArrow").click(function(){
    	var lth = $(".showgal").length;
        var n = currentSlide + 1;
        if(n>=lth){i=0}else{i=n}
        $("#img01").attr("src",$(".showgal:eq("+i+")").attr("src"));
        $("#caption").html($(".showgal:eq("+i+")").attr("alt"));
        currentSlide = i;
    });
    
    $(".leftArrow").click(function(){
        var lth = $(".showgal").length;
        var k = currentSlide - 1;
        if(k<0){i=lth-1}else{i=k}
        $("#img01").attr("src",$(".showgal:eq("+i+")").attr("src"));
        $("#caption").html($(".showgal:eq("+i+")").attr("alt"));
        currentSlide = i;
    });
});
</script>
</body>
</html>