<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
                $this->load->model('medsos/medsos_model','medsos_model');
                $this->load->model('berita_mmc/berita_model','berita_model');
                $this->load->model('gallery/gallery_model','gallery_model');
                $this->load->model('gallery/gallery_detail_model','gallery_detail_model');
                $this->load->model('kategori_berita/kategori_berita_model','kategori_berita_model');
                $this->load->model('video/video_model','video_model');
                $this->load->model('logo_banner/logo_banner_model','logo_banner_model');
        }

        public function index()
        {
            $data['logo_banner'] = $this->logo_banner_model->get("","row");
            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
        	$data['list_media_sosial'] = $this->medsos_model->get();
            $data['list_berita_acak'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "rand()"=>"desc"
                    ),
                    "limit"=>"4"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

            $data['list_berita_terbaru_carousel'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );

            $data['list_berita_seputar_kobar'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "kategori_berita_id"=>5,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

            $data['list_berita_nasional'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "kategori_berita_id"=>2,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

            $data['list_gallery'] = $this->gallery_model->get(
                array(
                    "fields"=>"gallery.*,(SELECT filename FROM gallery_detail WHERE gallery_id=id_gallery LIMIT 1) AS filename",
                    "order_by"=>array(
                        "tanggal_kegiatan"=>"desc"
                    ),
                    "limit"=>"10",
                    "group_by"=>"id_gallery"
                )
            );

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();
        	$data['content']='template_portal/home_content';
        	$data['url']=base_url();
        	$data['title']="Portal - Multimedia Center Kotawaringin Barat";
        	$data['description']="mmc.kotawaringinbaratkab.go.id - Multimedia Center Kotawaringin Barat, Berita Kotawaringin Barat Hari Ini, Kabar Terbaru Seputar Kobar, Nasional, Portal Berita Kobar, MMC Kobar";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
        	$data['image']=base_url()."images/logommc.jpg";
        	$this->load->view('template_portal/portalmmckobar',$data);
        }

        public function show_post_news($slug){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
            
            $data_master = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,user.nama_lengkap,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita",
                        "user"=>"user_id_created=id_user"
                    ),
                    "where"=>array(
                        "slug_berita"=>$slug
                    )
                ),"row"
            );

            if(!empty($data_master)){
                $data['berita'] = $data_master;
                // $jml = $data['berita']->readCount;
                // $this->Berita_model->updateReadCount(++$jml,$berita->idBerita);
                $data['content'] = 'template_portal/post_news';
                $data['url']=base_url()."berita/".$slug;
                $data['title']=$data_master->judul;
            	$data['keyword'] = str_replace(" ",",",$data_master->judul);
                $desc=strip_tags(html_entity_decode($data_master->deskripsi));
                $desc= preg_replace('/\s+?(\S+)?$/', '', substr($desc, 0, 101));
                $data['description']=$desc;
                $data['image']=base_url().$this->config->item('berita_path').$data_master->filename;
            }else{
                $data['content'] = 'template_portal/404_page';
                $data['url']=base_url();
                $data['title']="Portal - Multimedia Center Kotawaringin Barat";
                $data['description']="mmc.kotawaringinbaratkab.go.id - Multimedia Center Kotawaringin Barat, Berita Kotawaringin Barat Hari Ini, Kabar Terbaru Seputar Kobar, Nasional, Portal Berita Kobar, MMC Kobar";
            	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
                $data['image']=base_url()."images/logommc.jpg";
            }
            $this->load->view('template_portal/portalmmckobar', $data);
        }

        public function kategori($slug){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
            $data['kategori'] = $this->kategori_berita_model->get_by($slug);
        	$data['list_berita_acak_by_kategori'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "id_kategori_berita"=>$slug
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "rand()"=>"desc"
                    ),
                    "limit"=>"2"
                )
            );

        	$data['list_berita_slide_per_kategori'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "kategori_berita_id"=>$slug,
                    ),
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

        	$data['url']=base_url();
        	$data['title']=$data['kategori']->nama_kategori_berita." - Multimedia Center Kotawaringin Barat";
        	$data['description']="mmc.kotawaringinbaratkab.go.id - Multimedia Center Kotawaringin Barat, Berita Kotawaringin Barat Hari Ini, Kabar Terbaru Seputar Kobar, Nasional, Portal Berita Kobar, MMC Kobar";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
        	$data['image']=base_url()."images/logommc.jpg";
        	$limit_per_page = 20;
            $page = ($this->uri->segment(2)) ? ($this->uri->segment(2)-1) : 0;
            $total_records = $this->berita_model->get(
                array(
                    "fields"=>"count(*) as jml",
                    "where"=>array(
                        "is_active"=>1,
                        "kategori_berita_id"=>$slug
                    )
                ),"row"
            );

            $end = $page*$limit_per_page;

            if($end == 0){
                $data['berita'] = $this->berita_model->get(
                    array(
                        "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                        "join"=>array(
                            "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                        ),
                        "where"=>array(
                            "is_active"=>1,
                            "kategori_berita_id"=>$slug,
                        ),
                        "order_by"=>array(
                            "tanggal_publish_desc"=>"desc"
                        ),
                        "limit"=>$limit_per_page
                    )
                );
            }else{
                $data['berita'] = $this->berita_model->get(
                    array(
                        "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                        "join"=>array(
                            "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                        ),
                        "where"=>array(
                            "is_active"=>1,
                            "kategori_berita_id"=>$slug,
                        ),
                        "order_by"=>array(
                            "tanggal_publish_desc"=>"desc"
                        ),
                        "limit_arr"=>array($limit_per_page,$end)
                    )
                );
            }

            

            if ($total_records->jml > 0)
            {
                // get current page records
                     
                $config['base_url'] = base_url() . $data['kategori']->slug_kategori;
                $config['total_rows'] = $total_records->jml;
                $config['per_page'] = $limit_per_page;
                $config["uri_segment"] = 2;
                 
                // custom paging configuration
                $config['num_links'] = 6;
                $config['use_page_numbers'] = TRUE;
                $config['reuse_query_string'] = TRUE;
                 
                $config['full_tag_open'] = "<ul class='pagination'>";
                $config['full_tag_close'] ="</ul>";
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
                $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open'] = "<li>";
                $config['next_tagl_close'] = "</li>";
                $config['prev_tag_open'] = "<li>";
                $config['prev_tagl_close'] = "</li>";
                $config['first_tag_open'] = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open'] = "<li>";
                $config['last_tagl_close'] = "</li>";

                $config['first_link']='<< ';
                $config['last_link']='>> ';
                $config['next_link']='> ';
                $config['prev_link']='< ';
                 
                $this->pagination->initialize($config);
                     
                // build paging links
                $data["links"] = $this->pagination->create_links();
            }
            $data['content'] = 'template_portal/category';
            $this->load->view('template_portal/portalmmckobar', $data);
        }

		public function show_gallery_detail($slug){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
            $data['gallery'] = $this->gallery_model->get(
                array(
                    "where"=>array(
                        "slug_gallery"=>$slug
                    )
                ),"row"
            );
            $data['foto'] = $this->gallery_detail_model->get(
                array(
                    "where"=>array(
                        "gallery_id"=>$data['gallery']->id_gallery
                    )
                )
            );
        	$data['url']=base_url();
        	$data['title']=$data['gallery']->nama_gallery." - Multimedia Center Kotawaringin Barat";
        	$data['description']=$data['gallery']->nama_gallery.",Multimedia Center Kotawaringin Barat";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
        	$data['image']=base_url()."images/logommc.jpg";
            $data['content'] = 'template_portal/gallery_detail';
            $this->load->view('template_portal/portalmmckobar', $data);
        }

		public function gallery($slug){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
            $data['kategori'] = $this->kategori_berita_model->get_by($slug);
        	$data['list_berita_acak_by_kategori'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "id_kategori_berita"=>$slug
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "rand()"=>"desc"
                    ),
                    "limit"=>"2"
                )
            );

        	$data['list_berita_slide_per_kategori'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                        "kategori_berita_id"=>$slug,
                    ),
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
        	$data['url']=base_url();	
        	$limit_per_page = 10;
            $page = ($this->uri->segment(2)) ? ($this->uri->segment(2)-1) : 0;
        	$total_records = $this->gallery_model->get(
                array(
                    "fields"=>"count(*) as jml",
                    "where"=>array(
                        "kategori_foto"=>$slug,
                        "is_active"=>1
                    )
                ),"row"
            );

            $end = $page*$limit_per_page;

            if($end == 0){
                $data['album'] = $this->gallery_model->get(
                    array(
                        "fields"=>"gallery.*,DATE_FORMAT(tanggal_kegiatan, '%Y-%m-%d') AS tanggal_kegiatan,(SELECT filename FROM gallery_detail WHERE gallery_id=id_gallery LIMIT 1) AS filename",
                        "order_by"=>array(
                            "tanggal_kegiatan"=>"desc"
                        ),
                        "where"=>array(
                            "kategori_foto"=>$slug,
                            "is_active"=>1
                        ),
                        "limit"=>$limit_per_page,
                        "group_by"=>"id_gallery"
                    )
                );
            }else{
                $data['album'] = $this->gallery_model->get(
                    array(
                        "fields"=>"gallery.*,DATE_FORMAT(tanggal_kegiatan, '%Y-%m-%d') AS tanggal_kegiatan,(SELECT filename FROM gallery_detail WHERE gallery_id=id_gallery LIMIT 1) AS filename",
                        "order_by"=>array(
                            "tanggal_kegiatan"=>"desc"
                        ),
                        "where"=>array(
                            "kategori_foto"=>$slug,
                            "is_active"=>1
                        ),
                        "limit_arr"=>array($limit_per_page,$end),
                        "group_by"=>"id_gallery"
                    )
                );
            }
            
        	if ($total_records->jml > 0)
            {
                // get current page records
                     
                $config['base_url'] = base_url() .'kegiatan';
                $config['total_rows'] = $total_records->jml;
                $config['per_page'] = $limit_per_page;
                $config["uri_segment"] = 2;
                 
                // custom paging configuration
                $config['num_links'] = $total_records->jml;
                $config['use_page_numbers'] = TRUE;
                $config['reuse_query_string'] = TRUE;
                 
                $config['full_tag_open'] = "<ul class='pagination'>";
                $config['full_tag_close'] ="</ul>";
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
                $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open'] = "<li>";
                $config['next_tagl_close'] = "</li>";
                $config['prev_tag_open'] = "<li>";
                $config['prev_tagl_close'] = "</li>";
                $config['first_tag_open'] = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open'] = "<li>";
                $config['last_tagl_close'] = "</li>";

                $config['first_link']='< Pertama ';
                $config['last_link']='Terakhir > ';
                $config['next_link']='> ';
                $config['prev_link']='< ';
                 
                $this->pagination->initialize($config);
                     
                // build paging links
                $data["links"] = $this->pagination->create_links();
            }
        	$data['title']="Foto - Multimedia Center Kotawaringin Barat";
        	$data['description']="Foto,Gallery Foto,Multimedia Center Kotawaringin Barat";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
            $data['image']=base_url()."images/logommc.jpg";
            if($slug == '1'){
                $data['content'] = 'template_portal/gallery';
            }else{
                $data['content'] = 'template_portal/gallery_tempo_dulu';
            }
            $this->load->view('template_portal/portalmmckobar', $data);
        }

		public function video($slug){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

            $data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );

            $data['video'] = $this->video_model->get(
                array(
                    "fields"=>"video.*,DATE_FORMAT(tanggal_kegiatan, '%Y-%m-%d') AS tanggal_kegiatan",
                    "where"=>array(
                        "kategori_video"=>$slug
                    ),
                    "order_by"=>array(
                        "tanggal_kegiatan"=>"DESC"
                    )
                )
            );
        	$data['url']=base_url();
        	$data['title']="Video - Multimedia Center Kotawaringin Barat";
        	$data['description']="video,Multimedia Center Kotawaringin Barat";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
        	$data['image']=base_url()."images/logommc.jpg";
            $data['content'] = 'template_portal/video';
            $this->load->view('template_portal/portalmmckobar', $data);
        }

		public function search_news(){
            $data['logo_banner'] = $this->logo_banner_model->get("","row");

        	$kw = $this->input->post('search',true) != ""?$this->input->post('search',true):($this->session->userdata('search_kw')!=""?$this->session->userdata('search_kw'):"");
        	$data['list_kategori_menu'] = $this->kategori_berita_model->get(
                array(
                    "where_false"=>"id_kategori_berita != 7",
                    "order_by"=>array(
                        "urutan_kategori"=>"ASC"
                    )
                )
            );
            $data['list_media_sosial'] = $this->medsos_model->get();

            $data['list_kategori_footer'] = $this->kategori_berita_model->query("
            SELECT nama_kategori_berita,slug_kategori FROM kategori_berita
            UNION
            SELECT 'Foto Kegiatan','foto-kegiatan'
            ")->result();

            $data['list_berita_terbaru_footer'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "tanggal_publish_desc"=>"desc"
                    ),
                    "limit"=>"3"
                )
            );
            
            $data['list_berita_terpopuler'] = $this->berita_model->get(
                array(
                    "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                    "join"=>array(
                        "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                    ),
                    "where"=>array(
                        "is_active"=>1,
                    ),
                    "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                    "order_by"=>array(
                        "read_count"=>"desc"
                    ),
                    "limit"=>"10"
                )
            );
        	$data['url']=base_url();
        	$data['title']="Hasil Pencarian - Multimedia Center Kotawaringin Barat";
        	$data['description']="mmc.kotawaringinbaratkab.go.id - Multimedia Center Kotawaringin Barat, Berita Kotawaringin Barat Hari Ini, Kabar Terbaru Seputar Kobar, Nasional, Portal Berita Kobar, MMC Kobar";
        	$data['keyword'] = "portal, berita, kotawaringin, barat, diskominfo, kotawaringin barat, berita kotawaringin barat, info kotawaringin barat,berita terbaru, berita terpopuler, berita hari ini, berita harian kotawaringin barat, seputar kobar, berita kobar, mmc kobar, diskominfo kobar";
        	$data['image']=base_url()."images/logommc.jpg";
            $data['keyword'] = $kw;

            $limit_per_page = 20;
            
            $this->session->set_userdata('search_kw',$kw);
            
            $page = ($this->uri->segment(2)) ? ($this->uri->segment(2)-1) : 0;

            $total_records = $this->berita_model->get(
                array(
                    "fields"=>"count(*) as jml",
                    "where"=>array(
                        "is_active"=>1
                    ),
                    "where_false"=>"judul like '%".$kw."%'"
                ),"row"
            );
            
            $end = $page*$limit_per_page;

            if($end == 0){
                $data['berita'] = $this->berita_model->get(
                    array(
                        "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                        "join"=>array(
                            "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                        ),
                        "where"=>array(
                            "is_active"=>1,
                        ),
                        "where_false"=>"judul like '%".$kw."%'",
                        "order_by"=>array(
                            "tanggal_publish_desc"=>"desc"
                        ),
                        "limit"=>$limit_per_page
                    )
                );
            }else{
                $data['berita'] = $this->berita_model->get(
                    array(
                        "fields"=>"berita.*,kategori_berita.*,CONCAT(DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d'),' ',DATE_FORMAT(IFNULL(berita.updated_at,berita.created_at),'%H:%i:%s')) AS tanggal_publish_desc,DATE_FORMAT(IFNULL(set_tanggal_publish, berita.created_at), '%Y-%m-%d') AS tanggal_publish_show ",
                        "join"=>array(
                            "kategori_berita"=>"kategori_berita_id=id_kategori_berita"
                        ),
                        "where"=>array(
                            "is_active"=>1,
                        ),
                        "where_false"=>"judul like '%".$kw."%'",
                        "order_by"=>array(
                            "tanggal_publish_desc"=>"desc"
                        ),
                        "limit_arr"=>array($limit_per_page,$end)
                    )
                );
            }

            if ($total_records->jml > 0)
            {
                // get current page records
                     
                $config['base_url'] = base_url()."search";
                $config['total_rows'] = $total_records->jml;
                $config['per_page'] = $limit_per_page;
                $config["uri_segment"] = 2;
                 
                // custom paging configuration
                $config['num_links'] = 6;
                $config['use_page_numbers'] = TRUE;
                $config['reuse_query_string'] = TRUE;
                 
                $config['full_tag_open'] = "<ul class='pagination'>";
                $config['full_tag_close'] ="</ul>";
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
                $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open'] = "<li>";
                $config['next_tagl_close'] = "</li>";
                $config['prev_tag_open'] = "<li>";
                $config['prev_tagl_close'] = "</li>";
                $config['first_tag_open'] = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open'] = "<li>";
                $config['last_tagl_close'] = "</li>";

                $config['first_link']='<< ';
                $config['last_link']='>> ';
                $config['next_link']='> ';
                $config['prev_link']='< ';
                 
                $this->pagination->initialize($config);
                     
                // build paging links
                $data["links"] = $this->pagination->create_links();
            }
            $data['content'] = 'template_portal/search';
            $this->load->view('template_portal/portalmmckobar', $data);
        }
}

?>