<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
// require APPPATH . 'core/MY_Controller.php';

class Gate_user extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('user/user_model','user_model');
        $this->load->model('berita_mmc/berita_model','berita_model');
    }

    public function ceklogin_get(){
        $username = $this->get("username");
        $password = $this->get("password");
        $keyCodeMMC = $this->get("keyCodeMMC");

        $get_passw = $this->user_model->get(
            array(
                "fields"=>"password",
                "where"=>array(
                    "username"=>$username,
                    "key_code_api"=>$keyCodeMMC,
                    "is_active_user"=>"1"
                )
            ),"row"
        );

        if($get_passw){
            if(password_verify($password,$get_passw->password)){
                $user = $this->user_model->get(
                    array(
                        "fields"=>"id_user,nama_lengkap,nama_level_user",
                        "join"=>array(
                            "level_user"=>"id_level_user=level_user_id"
                        ),
                        "where"=>array(
                            "username"=>$username,
                            "is_active_user"=>"1"
                        )
                    ),"row"
                );
            }else{
                $user = array();
            }
        }else{
            $user = array();
        }


        if($user){
            $this->response([
                'status' => true,
                'data' => $user
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => $user
            ], REST_Controller::HTTP_OK);
        }
    }

	// public function postnews_post(){
    //     $judul = $this->post("judul");
    //     $caption = $this->post("caption");
    //     $tgglpublikasi = $this->post("tgglpublikasi");
    //     $userid = $this->post("userid");
    //     $isi = $this->post("isi");
    // 	$this->upload->initialize($this->set_upload_options());
    //     $this->upload->do_upload();

    //     $file_data = $this->upload->data();
    //     $filename = $file_data['file_name'];
    // 	$insert_news = $this->Gate_model->insert_news($judul,$tgglpublikasi,$isi,$userid);
    // 	$notif = $this->Gate_model->insert_image($insert_news,$filename,$caption);

    //     if($notif){
    //         $this->response([
    //             'status' => true,
    //             'message' => 'sukses',
    //         	'id_news_mmc' => $insert_news
    //         ], REST_Controller::HTTP_OK);
    //     }else{
    //         $this->response([
    //             'status' => false,
    //             'message' => 'gagal'
    //         ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

	public function savepublish_post(){
        $judul = $this->post("judul");
        $caption = $this->post("caption");
        $tgglpublikasi = $this->post("tggl");
        $userid = $this->post("userid");
        $isi = $this->post("isi");
    	$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $judul)));
        $cek_slug = $this->berita_model->get(
            array(
                "fields"=>"IFNULL(count(*),0) as jumlah_slug",
                "where"=>array(
                    "slug_berita"=>$slug,
                    "is_active"=>"1"
                )
            ),"row"
        );

        if($cek_slug->jumlah_slug != '0'){
            $slug .= $cek_slug->jumlah_slug;
        }

    	$input_name = 'userfile';
        $upload_foto = $this->upload_file($input_name,$this->config->item('berita_path'),'berita');

        $filename = $upload_foto['data']['file_name'];
        
        $data = array(
            "judul"=>$judul,
            "deskripsi"=>htmlentities($isi),
            "kategori_berita_id"=>'5',
            "set_tanggal_publish"=>date("Y-m-d H:i:s",strtotime($tgglpublikasi)),
            "filename"=>$filename,
            "caption"=>$caption,
            "slug_berita"=>$slug,
            "from_news"=>"bpkad",
            "user_id_created"=>$userid,
            "user_id_created"=>$userid,
            'created_at'=>$this->datetime()
        );

        $status = $this->berita_model->save($data);

        if($status){
            $this->response([
                'status' => true,
                'message' => 'sukses',
            	'id_news_mmc' => $status
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => $judul
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

	// public function trashnews_delete(){
    // 	$id_news_mmc = $this->delete("id_news_mmc");
    // 	$notif = $this->Gate_model->delete_news($id_news_mmc);
    //     if($notif){
    //         $this->response([
    //             'status' => true,
    //             'message' =>'sukses'
    //         ], REST_Controller::HTTP_OK);
    //     }else{
    //         $this->response([
    //             'status' => false,
    //             'message' => 'gagal'
    //         ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

	public function deletenews_get(){
        $id_berita = $this->get("id");
        $data_master = $this->berita_model->get_by($id_berita);

        if(is_file($this->config->item('berita_path')."/".$data_master->filename)){
            unlink($this->config->item('berita_path')."/".$data_master->filename);
        }
        
        if(is_file($this->config->item('berita_path')."/thumbnail/".$data_master->filename)){
            unlink($this->config->item('berita_path')."/thumbnail/".$data_master->filename);
        }

        $status = $this->berita_model->remove($id_berita);

        if($status){
            $this->response([
                'status' => true,
                'data' => 'success'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'failed'
            ], REST_Controller::HTTP_OK);
        }
    }

	// public function updatenews_put(){
    // 	$judul = $this->put("judul");
    //     $tgglpublikasi = $this->put("tgglpublikasi");
    // 	$caption = $this->put("caption");
    //     $userid = $this->put("userid");
    //     $isi = $this->put("isi");
    //     $idNewsMMc = $this->put("idNewsMMc");
    // 	$dataMaster = $this->Gate_model->getNewsById($idNewsMMc);
    // 	if(isset($_FILES['userfile']['name'])){
    //     	$this->upload->initialize($this->set_upload_options());
    //     	$this->upload->do_upload();

    //     	$file_data = $this->upload->data();
    //     	$filename = $file_data['file_name'];	
    //     }else{
    //     	$filename = $dataMaster->filename;
    //     }
    // 	$insert_news = $this->Gate_model->update_news($judul,$tgglpublikasi,$isi,$idNewsMMc);
    // 	$notif = $this->Gate_model->update_image($idNewsMMc,$filename,$caption);

    //     if($notif){
    //         $this->response([
    //             'status' => true,
    //             'message' => 'sukses',
    //         	'id_news_mmc' => $insert_news
    //         ], REST_Controller::HTTP_OK);
    //     }else{
    //         $this->response([
    //             'status' => false,
    //             'message' => 'gagal'
    //         ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

    public function upload_file($name_field = "",$upload_path = "",$upload_menu = "",$type_upload = "image"){
        if((!file_exists($upload_path)) && !(is_dir($upload_path))){
            mkdir($upload_path);
        }
        $config = array();
        $status = "";
        if($type_upload == 'image'){
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = $filename;
        }else{
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'docx|pdf|xlsx|ppt';
            $config['file_name'] = $filename;
        }

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name_field))
        {
                $status = array('error' => $this->upload->display_errors());
        }
        else
        {
                $status = array('data' => $this->upload->data());
                if($upload_menu == "berita"){
                    $this->thumbnailResizeImage($status['data']['file_name'],$upload_path);
                }
        }

        return $status;
    }

    public function thumbnailResizeImage($filename,$filepath)
    {
        $source_path = $filepath. '/' . $filename;
        $target_path = $filepath;

        if((!file_exists($target_path."/thumbnail")) && !(is_dir($target_path."/thumbnail"))){
            mkdir($target_path."/thumbnail");
        }
        
        $config = array(
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path."/thumbnail/".$filename,
                'maintain_ratio' => TRUE,
                'width' => 230  
            ),
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path."/".$filename,
                'maintain_ratio' => TRUE,
                'width' => 660  
            )
        );

        $this->load->library('image_lib', $config[0]);
        foreach($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

    public function datetime(){
        return $this->config->item('date_now');
    }

}

?>