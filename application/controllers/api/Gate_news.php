<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Gate_news extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('berita_mmc/berita_model','berita_model');
    }

    public function seputarkobar_get(){
        $data = $this->berita_model->get(
            array(
                "fields"=>"judul,concat('http://192.168.43.164/assets/foto_berita/thumbnail/',filename) as image_thumb,DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') AS tanggal_publish_show",
                "where"=>array(
                    "is_active"=>1,
                    "kategori_berita_id"=>5,
                ),
                "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()",
                "order_by"=>array(
                    "tanggal_publish_show"=>"desc"
                ),
                "limit"=>"10"
            )
        );

        if($data){
            $this->response([
                'status' => true,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => 'No Data'
            ], REST_Controller::HTTP_OK);
        }
    }

}

?>