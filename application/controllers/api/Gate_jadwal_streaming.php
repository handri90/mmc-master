<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Gate_jadwal_streaming extends REST_Controller
{

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('jadwal_kobar_tv/jadwal_kobar_tv_model', 'jadwal_kobar_tv_model');
    }

    public function jadwalkobar_get()
    {
        $today = date("Y-m-d");
        $data = $this->jadwal_kobar_tv_model->get(
            array(
                "fields" => "jadwal_kobar_tv.*,DATE_FORMAT(tanggal_jam, '%d-%m-%Y %h:%i:%s') AS tanggal_jam",
                "where_false" => "DATE_FORMAT(tanggal_jam, '%Y-%m-%d') = '" . $today . "'",
                "order_by" => array(
                    "tanggal_jam" => "ASC"
                )
            )
        );

        if ($data) {
            $this->response([
                'status' => true,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'data' => 'No Data'
            ], REST_Controller::HTTP_OK);
        }
    }
}
