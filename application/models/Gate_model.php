<?php
class Gate_model extends CI_Model {

        public function __construct()
        {
        	$this->load->database();
        }

        public function cek_user_login($username,$password,$keyCodeMMC)
        {
        		$cekPass = $this->db->query("select passMember from member where usernameMember='".$username."' and keyCode='".$keyCodeMMC."' and isActive='1'");
                $passCek = $cekPass->row();
        		if(isset($passCek->passMember)){
					if(password_verify($password,$passCek->passMember)){
                		$query = $this->db->query("select idMember as idUser,namaMember as namaUser,namaHakAkses as namaLevelUser from member inner join hakAkses on `hakAksesidHakAkses`=`idHakAkses` inner join listSKPD on idSKPD=skpdidskpd where usernameMember='".$username."'");
                		return $query->row();
                	}else{
                		return false;
                	}
        		}
        }

		public function insert_news($judul,$tgglpublikasi,$isi,$userid,$slug){
        		$query = $this->db->query("insert into berita (judul,tanggalPublish,kategoriIdKategori,content,memberIdMember,lastDate,slugBerita,log_news) values ('".$judul."',now(),5,'".$isi."',".$userid.",'".$tgglpublikasi."','".$slug."','Web BPKAD')");
        
        		return $this->db->insert_id();
		}

		public function insert_image($insert_news,$filename,$caption){
				$query = $this->db->query("insert into fotoBerita (filename,beritaIdBerita,caption) values ('".$filename."',".$insert_news.",'".$caption."')");
        
        		return $query;
		}

		public function insert_foto_tmp($filename){
			$query = $this->db->query("insert into foto_tmp (name) values ('".$filename."')");

                return $query;
		}

		public function delete_news($id_news_mmc){
        	$data = $this->db->query("select filename from fotoBerita where beritaIdBerita=".$id_news_mmc)->row();
			unlink("uploads/".$data->filename);
			$query = $this->db->query("delete from berita where idBerita = ".$id_news_mmc);
			return $query;
        }

		public function getNewsById($idNewsMMc){
        	$query = $this->db->query("select * from berita inner join fotoBerita on idBerita=beritaIdBerita where idBerita=".$idNewsMMc);
			return $query->row();
        }
	
		public function update_news($judul,$tgglpublikasi,$isi,$idNewsMMc){
        	$query = $this->db->query("update berita set judul='".$judul."',content='".$isi."',lastDate='".$tgglpublikasi."' where idBerita=".$idNewsMMc);
			return $query;
        }

		public function update_image($insert_news,$filename,$caption){
                $query = $this->db->query("update fotoBerita set filename='".$filename."',caption='".$caption."' where beritaIdBerita=".$insert_news);

                return $query;
		}

		public function get_news_terbaru(){
        	$query = $this->db->query("select idBerita,judul,concat('http://mmc.kotawaringinbaratkab.go.id/uploads/',filename) as filename,DATE_FORMAT(ifnull(CONCAT(lastDate,' ',DATE_FORMAT(ifnull(tanggalModified,tanggalPublish),'%H:%i:%s')),tanggalPublish), '%Y-%m-%d %H:%i:%s') as tanggalPublishOrder,DATE_FORMAT(ifnull(lastDate,tanggalPublish), '%Y-%m-%d') as tanggalPublishShow,namaKategori,readCount from berita left join fotoBerita on idBerita=beritaIdBerita left join kategori on kategoriIdKategori=idKategori where status= 1 order by tanggalPublishOrder desc limit 10");

                return $query->result_array();
        }
}
?>