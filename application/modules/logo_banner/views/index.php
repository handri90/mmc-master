<style>
.logo-image-custom > img{
	margin-bottom:10px;
}

.banner-header-image-custom > img{
	margin-bottom:10px;
}

.banner-content-image-custom > img{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
				<fieldset class="mb-3">
                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Logo <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<span class="logo-image-custom">
								<img width="200" src="<?php echo base_url().$this->config->item('logo_banner_path').'/'.$content->logo; ?>">
							</span>
							<?php
							}
							?>
							<input type="file" class="file-input" name="logo" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Banner Header <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<span class="banner-header-image-custom">
								<img width="500" src="<?php echo base_url().$this->config->item('logo_banner_path').'/'.$content->banner_header; ?>">
							</span>
							<?php
							}
							?>
							<input type="file" class="file-input" name="banner_header" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
						</div>
					</div>
                    
                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Banner Content <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<span class="banner-content-image-custom">
								<img width="500" src="<?php echo base_url().$this->config->item('logo_banner_path').'/'.$content->banner_content; ?>">
							</span>
							<?php
							}
							?>
							<input type="file" class="file-input" name="banner_content" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
						</div>
					</div>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>