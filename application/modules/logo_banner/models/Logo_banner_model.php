<?php

class Logo_banner_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="logo_banner";
        $this->primary_id="id_logo_banner";
    }
}