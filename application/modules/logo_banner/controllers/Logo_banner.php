<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logo_banner extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('logo_banner_model');
        $this->load->model('kategori_berita/kategori_berita_model','kategori_berita_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Logo & Banner','is_active'=>true]];

        $data_master = $this->logo_banner_model->get("","row");
        
        if(empty($_FILES)){
            $data['content'] = $data_master;
            $this->execute('index',$data);
        }else{
            
            $filename_logo = "";
            if($_FILES['logo']['name'] != ""){
                if($data_master){
                    unlink($this->config->item('logo_banner_path')."/".$data_master->logo);
                }
                $input_name_logo = 'logo';
                $upload_logo = $this->upload_file($input_name_logo,$this->config->item('logo_banner_path'));
                $filename_logo = $upload_logo['data']['file_name'];
            }else{
                $filename_logo = $data_master->logo;
            }
            
            $filename_banner_header = "";
            if($_FILES['banner_header']['name'] != ""){
                if($data_master){
                    unlink($this->config->item('logo_banner_path')."/".$data_master->banner_header);
                }
                $input_name_banner_header = 'banner_header';
                $upload_banner_header = $this->upload_file($input_name_banner_header,$this->config->item('logo_banner_path'));    
                $filename_banner_header = $upload_banner_header['data']['file_name'];
            }else{
                $filename_banner_header = $data_master->banner_header;
            }
            
            $filename_banner_content = "";
            if($_FILES['banner_content']['name'] != ""){
                if($data_master){
                    unlink($this->config->item('logo_banner_path')."/".$data_master->banner_content);
                }
                $input_name_banner_content = 'banner_content';
                $upload_banner_content = $this->upload_file($input_name_banner_content,$this->config->item('logo_banner_path'));
                $filename_banner_content = $upload_banner_content['data']['file_name'];
            }else{
                $filename_banner_content = $data_master->banner_content;
            }      
            
            
            if($data_master){
                $data = array(
                    "logo"=>$filename_logo,
                    "banner_header"=>$filename_banner_header,
                    "banner_content"=>$filename_banner_content,
                    'updated_at'=>$this->datetime()
                );
                
                $status = $this->logo_banner_model->edit($check->id_logo_banner,$data);
            }else{
                $data = array(
                    "logo"=>$filename_logo,
                    "banner_header"=>$filename_banner_header,
                    "banner_content"=>$filename_banner_content,
                    'created_at'=>$this->datetime()
                );
                
                $status = $this->logo_banner_model->save($data);
            }

            if($status){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('logo_banner');
        }
    }
}
