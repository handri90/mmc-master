<style>
.berita-image-custom > img{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
				<fieldset class="mb-3">
					<legend class="text-uppercase font-size-sm font-weight-bold">Berita</legend>
                    
                    <div class="form-group row">
						<label class="col-form-label col-lg-2">Judul <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->judul:""; ?>" name="judul" required placeholder="Judul">
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Tanggal Kegiatan <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control daterange-single" value="<?php echo !empty($content)?$content->tanggal_kegiatan:""; ?>" name="tanggal_kegiatan" required placeholder="Tanggal Kegiatan">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-lg-2">Link Youtube <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->link_youtube:""; ?>" name="link_youtube" required placeholder="Link Youtube">
						</div>
					</div>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>

$('.daterange-single').daterangepicker({ 
	singleDatePicker: true,
	locale: {
      format: 'DD/MM/YYYY'
    }
});
</script>