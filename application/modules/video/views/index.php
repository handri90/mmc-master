<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url().'video/tambah_video'; ?>" class="btn btn-info">Tambah Video</a>
            </div>
        </div>
        <table id="datatableVideo" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Video</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
let datatableVideo = $("#datatableVideo").DataTable({
    "columns":[
        {"width":"46"},
        null,
        null,
        {"width":"166"},
        {"width":"166"}
    ]
});
get_data_video();
function get_data_video(){
    datatableVideo.clear().draw();
    $.ajax({
            url: base_url+'video/request/get_data_video',
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableVideo.row.add([
                        no,
                        value.judul,
                        '<iframe src="'+value.link_youtube+'" frameborder="0" allowfullscreen></iframe>',
                        (value.is_active=='1'?'<span class="badge badge-success badge-'+value.id_video+'">Publish</span>':'<span class="badge badge-warning badge-'+value.id_video+'">Pending</span>'),
                        "<a href='"+base_url+"video/edit_video/"+value.id_encrypt+"' class='btn btn-primary btn-icon btn-sm'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon btn-sm' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>  <a class='btn btn-info btn-icon btn-sm' onClick=\"change_status('"+value.id_encrypt+"')\" href='#'><i class='icon-cloud'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
}

function confirm_delete(id_video){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'video/delete_video',
                data : {id_video:id_video},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_video();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_video();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_video();
                }
            });
        }
    });
}

function change_status(id_video){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin ingin merubah status video ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'video/change_status',
                data : {id_video:id_video},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_video();
                        swalInit(
                            'Berhasil',
                            'Status video berhasil diubah',
                            'success'
                        );
                    }else{
                        get_data_video();
                        swalInit(
                            'Gagal',
                            'Tidak bisa merubah status video',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_video();
                }
            });
        }
    });
}
</script>