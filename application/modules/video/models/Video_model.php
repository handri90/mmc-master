<?php

class Video_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="video";
        $this->primary_id="id_video";
    }
}