<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('video_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Video','is_active'=>true]];
        $this->execute('index',$data);
    }
    
    public function tambah_video(){
        if(empty($_POST)){
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'video','content'=>'Video','is_active'=>false],['link'=>false,'content'=>'Tambah Video','is_active'=>true]];
            $this->execute('form_video',$data);
        }else{
            $date_exp = explode("/",$this->ipost('tanggal_kegiatan'));
            $set_publish = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];

            $data = array(
                "judul"=>$this->ipost('judul'),
                "link_youtube"=>$this->ipost('link_youtube'),
                "tanggal_kegiatan"=>date("Y-m-d H:i:s",strtotime($set_publish)),
                "user_id_created"=>$this->session->userdata('id_user'),
                'created_at'=>$this->datetime()
            );

            $status = $this->video_model->save($data);
            if($status){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('video');
        }
    }

    public function edit_video($id_video){
        $data_master = $this->video_model->get(
            array(
                "fields"=>"video.*,DATE_FORMAT(video.tanggal_kegiatan,'%d/%m/%Y') AS tanggal_kegiatan",
                "where"=>array(
                    "id_video"=>decrypt_data($id_video)
                )
            ),"row"
        );

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'video','content'=>'Video','is_active'=>false],['link'=>false,'content'=>'Tambah Video','is_active'=>true]];
            $this->execute('form_video',$data);
        }else{

            $date_exp = explode("/",$this->ipost('tanggal_kegiatan'));
            $set_publish = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];
            
            $data = array(
                "judul"=>$this->ipost('judul'),
                "link_youtube"=>$this->ipost('link_youtube'),
                "tanggal_kegiatan"=>date("Y-m-d H:i:s",strtotime($set_publish)),
                'updated_at'=>$this->datetime()
            );

            $status = $this->video_model->edit(decrypt_data($id_video),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('video');
        }
    }

    public function delete_video(){
        $id_video = $this->iget('id_video');
        $data_master = $this->video_model->get_by(decrypt_data($id_video));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->video_model->remove(decrypt_data($id_video));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function change_status(){
        $id_video = $this->iget('id_video');
        $data_master = $this->video_model->get_by(decrypt_data($id_video));
        if(!$data_master){
            $this->page_error();
        }

        if($data_master->is_active == '1'){
            $change_data = array(
                "is_active"=>"0"
            );
        }else{
            $change_data = array(
                "is_active"=>"1"
            );
        }

        $status = $this->video_model->edit(decrypt_data($id_video),$change_data);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
