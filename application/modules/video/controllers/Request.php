<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('video_model');
	}

	function get_data_video(){
        $data_video = $this->video_model->get(
            array(
                "fields"=>"video.*,DATE_FORMAT(IFNULL(tanggal_kegiatan,video.created_at), '%Y-%m-%d') AS tanggal_kegiatan",
                "order_by"=>array(
                    "tanggal_kegiatan"=>"DESC"
                )
            )
        );

        $templist = array();
        foreach($data_video as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_video);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
