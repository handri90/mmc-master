<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('jadwal_kobar_tv_model');
    }

    function get_data_jadwal()
    {
        $data_jadwal = $this->jadwal_kobar_tv_model->get();

        $templist = array();
        foreach ($data_jadwal as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_jadwal);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
