<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_kobar_tv extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('jadwal_kobar_tv_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Jadwal Kobar TV', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_jadwal()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_kobar_tv', 'content' => 'Jadwal Kobar TV', 'is_active' => false], ['link' => false, 'content' => 'Tambah Jadwal', 'is_active' => true]];
            $this->execute('form_jadwal', $data);
        } else {

            $tanggal_jam = explode(" ", $this->ipost("tanggal_jam"));
            $tanggal_menyerahkan = explode("-", $tanggal_jam[0]);

            $data = array(
                "deskripsi" => $this->ipost('deskripsi'),
                "tanggal_jam" => $tanggal_menyerahkan[2] . "-" . $tanggal_menyerahkan[1] . "-" . $tanggal_menyerahkan[0] . " " . $tanggal_jam[1],
                'created_at' => $this->datetime()
            );

            $status = $this->jadwal_kobar_tv_model->save($data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('jadwal_kobar_tv');
        }
    }

    public function edit_jadwal($id_jadwal)
    {
        $data_master = $this->jadwal_kobar_tv_model->get(
            array(
                "fields" => "jadwal_kobar_tv.*,DATE_FORMAT(tanggal_jam,'%d-%m-%Y %h:%i:%s') as tanggal_jam",
                "where" => array(
                    "id_jadwal" => decrypt_data($id_jadwal)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'jadwal_kobar_tv', 'content' => 'Jadwal Kobar TV', 'is_active' => false], ['link' => false, 'content' => 'Tambah Jadwal', 'is_active' => true]];
            $this->execute('form_jadwal', $data);
        } else {
            $tanggal_jam = explode(" ", $this->ipost("tanggal_jam"));
            $tanggal_menyerahkan = explode("-", $tanggal_jam[0]);

            $data = array(
                "deskripsi" => $this->ipost('deskripsi'),
                "tanggal_jam" => $tanggal_menyerahkan[2] . "-" . $tanggal_menyerahkan[1] . "-" . $tanggal_menyerahkan[0] . " " . $tanggal_jam[1],
                'updated_at' => $this->datetime()
            );

            $status = $this->jadwal_kobar_tv_model->edit(decrypt_data($id_jadwal), $data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('jadwal_kobar_tv');
        }
    }

    public function delete_jadwal()
    {
        $id_jadwal = $this->iget('id_jadwal');
        $data_master = $this->jadwal_kobar_tv_model->get_by(decrypt_data($id_jadwal));

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->jadwal_kobar_tv_model->remove(decrypt_data($id_jadwal));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
