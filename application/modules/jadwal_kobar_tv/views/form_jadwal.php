<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open(); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">Jadwal Kobar TV</legend>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Tanggal / Jam <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->tanggal_jam : date("d-m-Y H:i:s"); ?>" name="tanggal_jam" id="anytime-both" readonly placeholder="Tanggal/Jam">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Deskripsi <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->deskripsi : ""; ?>" name="deskripsi" required placeholder="Deskripsi">
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
	$(function() {
		$('#anytime-both').AnyTime_picker({
			format: '%d-%m-%Y %H:%i:%s',
		});
	})
</script>