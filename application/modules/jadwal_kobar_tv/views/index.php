<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'jadwal_kobar_tv/tambah_jadwal'; ?>" class="btn btn-info">Tambah Jadwal</a>
            </div>
        </div>
        <table id="datatableJadwalTV" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal/Jam</th>
                    <th>Deskripsi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableJadwalTV = $("#datatableJadwalTV").DataTable({
        "columns": [
            null,
            null,
            {
                "width": "136"
            }
        ]
    });
    get_data_jadwal();

    function get_data_jadwal() {
        datatableJadwalTV.clear().draw();
        $.ajax({
            url: base_url + 'jadwal_kobar_tv/request/get_data_jadwal',
            type: 'GET',
            beforeSend: function() {
                loading_start();
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableJadwalTV.row.add([
                        value.tanggal_jam,
                        value.deskripsi,
                        "<a href='" + base_url + "jadwal_kobar_tv/edit_jadwal/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                loading_stop();
            }
        });
    }

    function confirm_delete(id_jadwal) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'jadwal_kobar_tv/delete_jadwal',
                    data: {
                        id_jadwal: id_jadwal
                    },
                    type: 'GET',
                    beforeSend: function() {
                        loading_start();
                    },
                    success: function(response) {
                        if (response) {
                            get_data_jadwal();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_jadwal();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        loading_stop();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    loading_stop();
                    if (result.results) {
                        get_data_jadwal();
                    }
                });
            }
        });
    }
</script>