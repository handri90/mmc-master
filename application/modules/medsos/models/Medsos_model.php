<?php

class Medsos_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="medsos";
        $this->primary_id="id_medsos";
    }
}