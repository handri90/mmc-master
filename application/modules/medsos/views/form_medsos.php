<style>
.user-image-custom{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
				<fieldset class="mb-3">
					<legend class="text-uppercase font-size-sm font-weight-bold">Gallery</legend>

                    <div class="form-group row">
						<label class="col-form-label col-lg-2">Kategori Gallery <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<select name="id_kategori" class="form-control select-search" required>
								<option value="">-- Kategori Gallery --</option>
								<?php
								foreach($kategori_gallery as $key=>$row){
                                    $selected = "";
                                    if(isset($content)){
                                        if($content->kategori_foto == $key){
                                            $selected = 'selected="selected"';
                                        }
                                    }
									?>
									<option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Nama Gallery <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->nama_gallery:""; ?>" name="nama_gallery" required placeholder="Nama Gallery">
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-lg-2">Tanggal Kegiatan <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control daterange-single" value="<?php echo !empty($content)?$content->tanggal_kegiatan:""; ?>" name="tanggal_kegiatan" required placeholder="Tanggal Kegiatan">
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-lg-2 col-form-label">Foto <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
                                ?>
                                <table border="1" style="margin-bottom:10px">
                                <?php
                                $expl_filename = explode('|',$content->filename);
                                $expl_id_gallery_detail = explode('|',$content->id_gallery_detail);
                                foreach($expl_filename as $key=>$val){
                                ?>
                                    <tr>
                                        <td style="padding:10px;"><img width="200" src="<?php echo base_url().$this->config->item('gallery_path')."/".$val; ?>"></td>
                                        <td width="100" style="padding:10px;vertical-align:center;text-align:center;"><a class='btn btn-danger btn-icon' onClick="confirm_delete('<?php echo encrypt_data($expl_id_gallery_detail[$key]); ?>',this)" href='#'><i class='icon-trash'></i></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </table>
                                <?php
							}
							?>
							<input type="file" class="file-input" multiple="multiple" name="foto_berita[]" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
						</div>
					</div>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
$('.daterange-single').daterangepicker({ 
	singleDatePicker: true,
	locale: {
      format: 'DD/MM/YYYY'
    }
});

function confirm_delete(id_gallery_detail,elm){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus gambar ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'gallery/delete_gallery_detail',
                data : {id_gallery_detail:id_gallery_detail},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        $(elm).parent().parent().remove();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            )
        }
    });
}
</script>