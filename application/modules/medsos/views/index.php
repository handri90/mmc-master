<style>
.user-image-custom{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
				<fieldset class="mb-3">
					<legend class="text-uppercase font-size-sm font-weight-bold">Medsos</legend>
                    <?php
                    foreach($kategori_medsos as $key=>$val){
                        $val_medsos = "";
                        foreach($list_medsos as $key_list=>$val_list){
                            if($val == $val_list->nama_medsos){
                                $val_medsos = $val_list->link_medsos;
                                break;
                            }
                        }
                        ?>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2"><?php echo $val; ?> <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input name="link_medsos[<?php echo $val; ?>]" type="text" class="form-control" value="<?php echo $val_medsos; ?>" placeholder="Link Medsos">
                            </div>
                        </div>
                        <?php
                    }
                    ?>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>