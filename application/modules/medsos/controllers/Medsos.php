<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medsos extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('medsos_model');
	}
    
    public function index(){
        $data["kategori_medsos"] = array(
            "facebook",
            "twitter",
            "youtube",
            "instagram"
        );

        $data['list_medsos'] = $this->medsos_model->get();

        if(empty($_POST)){
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'medsos','content'=>'Medsos','is_active'=>false],['link'=>false,'content'=>'Tambah Medsos','is_active'=>true]];
            $this->execute('index',$data);
        }else{
            foreach($data["kategori_medsos"] as $key=>$val){
                $data = array(
                    "nama_medsos"=>$val,
                    "link_medsos"=>$this->ipost('link_medsos')[$val],
                    'created_at'=>$this->datetime()
                );

                $check = $this->medsos_model->get(
                    array(
                        "where"=>array(
                            "nama_medsos"=>$val
                        )
                    ),"row"
                );

                if($check){
                    $status = $this->medsos_model->edit($check->id_medsos,$data);
                }else{
                    $status = $this->medsos_model->save($data);
                }
    
            }
            
            if($status){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('medsos');
        }
    }
}
