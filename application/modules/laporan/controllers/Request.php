<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('berita_mmc/berita_model', 'berita_model');
    }

    function get_data_laporan()
    {
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");

        $kategori_berita = decrypt_data($this->iget("kategori_berita"));

        $wh = "";
        if ($kategori_berita) {
            $wh = "AND kategori_berita_id = " . $kategori_berita;
        }

        $data = $this->berita_model->query("
        SELECT id_user,nama_lengkap,IFNULL(jumlah_berita,0) AS jumlah_berita,IFNULL(jumlah_pembaca,0) AS jumlah_pembaca,IFNULL(ROUND(jumlah_pembaca/jumlah_berita,2),0) AS rata_rata_pembaca,
        IFNULL(
        ROUND(((jumlah_berita)/(SELECT COUNT(id_berita) jumlah_berita
        FROM berita 
        WHERE is_active = '1' 
        " . $wh . " 
        AND berita.`deleted_at` IS NULL 
        AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'))*100,2),0
        ) AS persentase
        FROM user
        LEFT JOIN (
            SELECT COUNT(id_berita) jumlah_berita,user_id_created,SUM(read_count) AS jumlah_pembaca
            FROM berita 
            WHERE is_active = '1' " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'
            GROUP BY user_id_created
            )AS a ON a.user_id_created=id_user
        ORDER BY jumlah_berita DESC
        ")->result();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
