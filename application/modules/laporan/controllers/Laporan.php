<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("kategori_berita/kategori_berita_model", "kategori_berita_model");
	}

	public function index()
	{
		$data['list_kategori_berita'] = $this->kategori_berita_model->get(
			array(
				"order_by" => array(
					"nama_kategori_berita" => "ASC"
				)
			)
		);

		$data['breadcrumb'] = [['link' => false, 'content' => 'Laporan', 'is_active' => true]];
		$this->execute('index', $data);
	}

	public function cetak_laporan($id_kategori_berita, $start_date, $end_date)
	{
		require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		$filename = "Laporan Per Tahun.xlsx";

		$objPHPExcel = new PHPExcel();

		$expl_start_date = explode("-", $start_date);
		$expl_end_date = explode("-", $end_date);

		$wh = "";
		if ($id_kategori_berita != "all") {
			$data_kategori = $this->kategori_berita_model->get(
				array(
					"where" => array(
						"id_kategori_berita" => decrypt_data($id_kategori_berita)
					)
				),
				"row"
			);

			$nama_kategori = $data_kategori->nama_kategori_berita;

			$wh = "AND kategori_berita_id = " . $kategori_berita;
		} else {
			$nama_kategori = "SEMUA";
		}

		$data_laporan = $this->kategori_berita_model->query("
        SELECT id_user,nama_lengkap,IFNULL(jumlah_berita,0) AS jumlah_berita,IFNULL(jumlah_pembaca,0) AS jumlah_pembaca,IFNULL(ROUND(jumlah_pembaca/jumlah_berita,2),0) AS rata_rata_pembaca,
        IFNULL(
        ROUND(((jumlah_berita)/(SELECT COUNT(id_berita) jumlah_berita
        FROM berita 
        WHERE is_active = '1' 
        " . $wh . " 
        AND berita.`deleted_at` IS NULL 
        AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "'))*100,2),0
        ) AS persentase
        FROM user
        LEFT JOIN (
            SELECT COUNT(id_berita) jumlah_berita,user_id_created,SUM(read_count) AS jumlah_pembaca
            FROM berita 
            WHERE is_active = '1' " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "'
            GROUP BY user_id_created
            )AS a ON a.user_id_created=id_user
        ORDER BY jumlah_berita DESC
        ")->result();

		$style_header_table = array(
			'font' => array(
				'size' => 11,
				'bold' => true
			), 'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$style_header_child = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'font' => array(
				'size' => 9,
			), 'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'e7e5e6')
			)
		);

		$style_data = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$objPHPExcel->getActiveSheet()->SetCellValue("A1", "REKAP JUMLAH BERITA YANG PUBLISH/TAYANG DI");
		$objPHPExcel->getActiveSheet()->SetCellValue("A2", "MMC.KOTAWARINGINBARATKAB.GO.ID PER TANGGAL " . date_indo($end_date));
		$objPHPExcel->getActiveSheet()->SetCellValue("A3", "Kategori Berita : " . $nama_kategori);

		$objPHPExcel->getActiveSheet()->mergeCells("A1:F1");
		$objPHPExcel->getActiveSheet()->mergeCells("A2:F2");
		$objPHPExcel->getActiveSheet()->mergeCells("A3:F3");

		$objPHPExcel->getActiveSheet()->getStyle("A1:F3")->applyFromArray($style_header_table);

		$objPHPExcel->getActiveSheet()->SetCellValue("A6", "NO");
		$objPHPExcel->getActiveSheet()->SetCellValue("B6", "KONTRIBUTOR");
		$objPHPExcel->getActiveSheet()->SetCellValue("C6", "BERITA PUBLISH");
		$objPHPExcel->getActiveSheet()->SetCellValue("D6", "JUMLAH PEMBACA");
		$objPHPExcel->getActiveSheet()->SetCellValue("E6", "RATA-RATA (PEMBACA/BERITA)");
		$objPHPExcel->getActiveSheet()->SetCellValue("F6", "KONTRIBUSI");

		$objPHPExcel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style_header_child);

		$number_column = 7;
		$no_data = 1;
		foreach ($data_laporan as $key => $row) {

			$objPHPExcel->getActiveSheet()->SetCellValue("A" . $number_column, $no_data);
			$objPHPExcel->getActiveSheet()->getStyle("A" . $number_column)->applyFromArray($style_data);

			$objPHPExcel->getActiveSheet()->SetCellValue("B" . $number_column, $row->nama_lengkap);
			$objPHPExcel->getActiveSheet()->getStyle("B" . $number_column)->applyFromArray($style_data);

			$objPHPExcel->getActiveSheet()->SetCellValue("C" . $number_column, $row->jumlah_berita);
			$objPHPExcel->getActiveSheet()->getStyle("C" . $number_column)->applyFromArray($style_data);

			$objPHPExcel->getActiveSheet()->SetCellValue("D" . $number_column, $row->jumlah_pembaca);
			$objPHPExcel->getActiveSheet()->getStyle("D" . $number_column)->applyFromArray($style_data);

			$objPHPExcel->getActiveSheet()->SetCellValue("E" . $number_column, $row->rata_rata_pembaca);
			$objPHPExcel->getActiveSheet()->getStyle("E" . $number_column)->applyFromArray($style_data);

			$objPHPExcel->getActiveSheet()->SetCellValue("F" . $number_column, $row->persentase);
			$objPHPExcel->getActiveSheet()->getStyle("F" . $number_column)->applyFromArray($style_data);

			$no_data++;
			$number_column++;
		}

		$jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while (true) {
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
				break;
			}
		}

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		$writer->save('php://output');
		exit;
	}
}
