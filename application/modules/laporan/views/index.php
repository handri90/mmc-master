<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal : </label>
                        <button type="button" class="btn btn-light daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                            <input type="hidden" name="startDate" />
                            <input type="hidden" name="endDate" />
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kategori Berita : </label>
                        <select class="form-control select-search" name="kategori_berita" onChange="get_data_laporan()">
                            <option value="">-- SEMUA --</option>
                            <?php
                            foreach ($list_kategori_berita as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_kategori_berita); ?>"><?php echo $row->nama_kategori_berita; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="#cetakLaporan" id="cetakLaporan" target="_blank" class="btn btn-info">Cetak Laporan</a>
            </div>
        </div>
        <table id="datatableLaporan" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kontributor</th>
                    <th>Berita Publish</th>
                    <th>Jumlah Pembaca</th>
                    <th>Rata-rata (Pembaca/berita)</th>
                    <th>Kontribusi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    let datatableLaporan = $("#datatableLaporan").DataTable();

    function get_data_laporan() {
        let startDate = $("input[name=startDate]").val();
        let endDate = $("input[name=endDate]").val();
        let kategori_berita = $("select[name=kategori_berita]").val();

        let parseKategoriBerita = "";
        if (kategori_berita == "") {
            parseKategoriBerita = "all";
        } else {
            parseKategoriBerita = kategori_berita;
        }

        $("#cetakLaporan").attr("href", base_url + "laporan/cetak_laporan/" + parseKategoriBerita + "/" + startDate + "/" + endDate);

        datatableLaporan.clear().draw();
        $.ajax({
            url: base_url + 'laporan/request/get_data_laporan',
            data: {
                kategori_berita: kategori_berita,
                startDate: startDate,
                endDate: endDate
            },
            type: 'GET',
            beforeSend: function() {
                loading_start();
            },
            success: function(response) {
                let no = 1;
                $.each(response, function(index, value) {
                    datatableLaporan.row.add([
                        no++,
                        value.nama_lengkap,
                        value.jumlah_berita,
                        value.jumlah_pembaca,
                        value.rata_rata_pembaca,
                        value.persentase
                    ]).draw(false);
                });
            },
            complete: function() {
                loading_stop();
            }
        });
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2018',
            maxDate: '12/31/2022',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $("input[name=startDate]").val(start.format('YYYY-MM-DD'));
            $("input[name=endDate]").val(end.format('YYYY-MM-DD'));
            $(".show_tanggal").html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));

            get_data_laporan();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $("input[name=startDate]").val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $("input[name=endDate]").val(moment().format('YYYY-MM-DD'));
    $(".show_tanggal").html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    get_data_laporan();
</script>