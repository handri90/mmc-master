<style>
.user-image-custom{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(current_url(),array('class'=>'form-validate-jquery')); ?>
				<fieldset class="mb-3">
					<legend class="text-uppercase font-size-sm font-weight-bold">Kategori Berita</legend>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Nama Kategori <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->nama_kategori_berita:""; ?>" name="nama_kategori_berita" required placeholder="Nama Kategori">
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Urutan Kategori <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->urutan_kategori:""; ?>" name="urutan_kategori" required placeholder="Urutan Kategori">
						</div>
					</div>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>