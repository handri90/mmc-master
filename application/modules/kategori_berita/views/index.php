<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url().'kategori_berita/tambah_kategori_berita'; ?>" class="btn btn-info">Tambah Kategori</a>
            </div>
        </div>
        <table id="datatableKategoriBerita" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama Kategori</th>
                    <th>Urutan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
let datatableKategoriBerita = $("#datatableKategoriBerita").DataTable({
    "columns":[
        null,
        {"width":"46"},
        {"width":"136"}
    ]
});
get_data_kategori_berita();
function get_data_kategori_berita(){
    datatableKategoriBerita.clear().draw();
    $.ajax({
            url: base_url+'kategori_berita/request/get_data_kategori_berita',
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                $.each(response,function(index,value){
                    datatableKategoriBerita.row.add([
                        value.nama_kategori_berita,
                        value.urutan_kategori,
                        "<a href='"+base_url+"kategori_berita/edit_kategori_berita/"+value.id_encrypt+"' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete:function(){
                loading_stop();
            }
        });
}

function confirm_delete(id_kategori_berita){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'kategori_berita/delete_kategori_berita',
                data : {id_kategori_berita:id_kategori_berita},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_kategori_berita();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_kategori_berita();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_kategori_berita();
                }
            });
        }
    });
}
</script>