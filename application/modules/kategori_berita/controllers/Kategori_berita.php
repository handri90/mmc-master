<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_berita extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('kategori_berita_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Kategori Berita','is_active'=>true]];
        $this->execute('index',$data);
    }
    
    public function tambah_kategori_berita(){
        if(empty($_POST)){
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'kategori_berita','content'=>'Kategori Berita','is_active'=>false],['link'=>false,'content'=>'Tambah Kategori Berita','is_active'=>true]];
            $this->execute('form_kategori_berita',$data);
        }else{

            $data = array(
                "nama_kategori_berita"=>$this->ipost('nama_kategori_berita'),
                "urutan_kategori"=>$this->ipost('urutan_kategori'),
                'created_at'=>$this->datetime()
            );

            $status = $this->kategori_berita_model->save($data);
            if($status){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('kategori_berita');
        }
    }

    public function edit_kategori_berita($id_kategori_berita){
        $data_master = $this->kategori_berita_model->get_by(decrypt_data($id_kategori_berita));

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'kategori_berita','content'=>'Kategori Berita','is_active'=>false],['link'=>false,'content'=>'Tambah Kategori Berita','is_active'=>true]];
            $this->execute('form_kategori_berita',$data);
        }else{
            $data = array(
                "nama_kategori_berita"=>$this->ipost('nama_kategori_berita'),
                "urutan_kategori"=>$this->ipost('urutan_kategori'),
                'updated_at'=>$this->datetime()
            );

            $status = $this->kategori_berita_model->edit(decrypt_data($id_kategori_berita),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('kategori_berita');
        }
    }

    public function delete_kategori_berita(){
        $id_kategori_berita = $this->iget('id_kategori_berita');
        $data_master = $this->kategori_berita_model->get_by(decrypt_data($id_kategori_berita));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->kategori_berita_model->remove(decrypt_data($id_kategori_berita));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
