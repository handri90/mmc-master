<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal Awal: </label>
						<button type="button" class="btn btn-light daterange-predefined">
                            <i class="icon-calendar22 mr-2"></i>
                            <span></span>
                            <input type="hidden" name="startDate" />
                            <input type="hidden" name="endDate" />
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url().'berita_mmc/tambah_berita'; ?>" class="btn btn-info">Tambah Berita</a>
            </div>
        </div>
        <table id="datatableBerita" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Penulis</th>
                    <th>Tanggal Kegiatan</th>
                    <th>Judul Berita</th>
                    <th>Status</th>
                    <th>Jumlah Pembaca</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
let datatableBerita = $("#datatableBerita").DataTable({
    "columns":[
        {"width":"46"},
        {"width":"186"},
        {"width":"186"},
        null,
        {"width":"46"},
        {"width":"46"},
        {"width":"186"}
    ]
});

function get_data_berita(){
    let startDate = $("input[name=startDate]").val();
    let endDate = $("input[name=endDate]").val();

    datatableBerita.clear().draw();
    $.ajax({
            url: base_url+'berita_mmc/request/get_data_berita',
            data : {startDate:startDate,endDate:endDate},
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableBerita.row.add([
                        no,
                        value.nama_lengkap,
                        value.tanggal_kegiatan_custom,
                        value.judul_custom,
                        (value.is_active=='1'?'<span class="badge badge-success badge-'+value.id_berita+'">Publish</span>':'<span class="badge badge-warning badge-'+value.id_berita+'">Pending</span>'),
                        value.read_count,
                        "<a title='Edit Berita' href='"+base_url+"berita_mmc/edit_berita/"+value.id_encrypt+"' class='btn btn-primary btn-icon btn-sm'><i class='icon-pencil7'></i></a> <a title='Delete Berita' class='btn btn-danger btn-icon btn-sm' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a> <a title='Status Publish' class='btn btn-info btn-icon btn-sm' onClick=\"change_status('"+value.id_encrypt+"')\" href='#'><i class='icon-cloud'></i></a> "+(value.is_active=='0'?"":"<a title='View Berita' target='_blank' href='"+base_url+"berita/"+value.slug_berita+"' class='btn bg-indigo btn-icon btn-sm'><i class='icon-eye8'></i></a>")
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
}

function confirm_delete(id_berita){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'berita_mmc/delete_berita',
                data : {id_berita:id_berita},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_berita();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_berita();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_berita();
                }
            });
        }
    });
}

function change_status(id_berita){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin ingin merubah status berita ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'berita_mmc/change_status',
                data : {id_berita:id_berita},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        swalInit(
                            'Berhasil',
                            'Status publish berubah!',
                            'success'
                        ).then(function(results){
                            if(results){
                                get_data_berita();
                            }
                        });
                        if(response.from_news == "bpkad"){
                            var fd = new FormData();
                            fd.append('idnewsmmc',response.id_berita);
                            fd.append('judul',response.judul);
                            fd.append('tggl',response.tanggal_publish);
                            fd.append('status',response.is_active);
                            fd.append('isi',response.deskripsi);

                            $.ajax({
                                type: "POST",
                                url: "http://bpkad.kotawaringinbaratkab.go.id/api/Tarot/publishupdate", 
                                data:fd,
                                beforeSend: function(){
                                    // loading_start();
                                },
                                success: function(response_api){
                                    console.log(response_api);
                                },
                                complete:function(response_api){
                                    // loading_stop();
                                }
                            });
                        }
                    }else{
                        get_data_berita();
                        swalInit(
                            'Gagal',
                            'Tidak bisa merubah status berita',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    if(response.from_news == ""){
                        loading_stop();
                    }
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_berita();
                }
            });
        }
    });
}

$('.daterange-predefined').daterangepicker(
    {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2018',
        maxDate: '12/31/2022',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        applyClass: 'btn-sm btn-primary',
        cancelClass: 'btn-sm btn-light'
    },
    function(start, end) {
        $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
        $("input[name=startDate]").val(start.format('YYYY-MM-DD'));
        $("input[name=endDate]").val(end.format('YYYY-MM-DD'));
        get_data_berita();
    }
);

// Display date format
$('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
$("input[name=startDate]").val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
$("input[name=endDate]").val(moment().format('YYYY-MM-DD'));
get_data_berita();
</script>