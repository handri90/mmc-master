<style>
.berita-image-custom > img{
	margin-bottom:10px;
}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
				<fieldset class="mb-3">
					<legend class="text-uppercase font-size-sm font-weight-bold">Berita</legend>

					<div class="form-group row">
						<label class="col-form-label col-lg-2">Kategori <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<select name="id_kategori" class="form-control select-search" required>
								<option value="">-- Kategori --</option>
								<?php
								foreach($kategori_berita as $key=>$row){
									$selected = "";
									if(!empty($content)){
										if($content->kategori_berita_id == $row->id_kategori_berita){
											$selected = 'selected="selected"';
										}
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_kategori_berita); ?>"><?php echo $row->nama_kategori_berita; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Judul <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" value="<?php echo !empty($content)?$content->judul:""; ?>" name="judul" required placeholder="Judul">
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Tanggal Kegiatan <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" class="form-control daterange-single" value="<?php echo !empty($content)?$content->set_tanggal_publish:""; ?>" name="set_tanggal_publish" required placeholder="Tanggal Kegiatan">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-lg-2">Content <span class="text-danger">*</span></label>
						<div class="col-lg-10">
                            <textarea name="content" class="form-control" id="editor" rows="6"><?php echo !empty($content)?$content->deskripsi:""; ?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-2 col-form-label">Foto <?php echo !empty($content)?'':'<span class="text-danger">*</span>'; ?></label>
						<div class="col-lg-10">
							<?php
							if(!empty($content)){
							?>
							<span class="berita-image-custom">
								<img width="300" src="<?php echo base_url().'assets/foto_berita/'.$content->filename; ?>">
								<input name='caption' value="<?php echo !empty($content)?$content->caption:""; ?>" type='text' class='form-control' placeholder='Caption' /><br>
							</span>
							<?php
							}
							?>
							<input type="file" class="file-input" name="foto_berita" <?php echo !empty($content)?'':'required'; ?> data-show-upload="false">
						</div>
					</div>
				</fieldset>

				<div class="text-right">
					<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
$(".file-input").on('change',function(){
	$(".berita-image-custom").remove();
	$(".file-preview").after("<input name='caption' type='text' class='form-control' placeholder='Caption' /><br>");
});

CKEDITOR.replace( 'editor',{
	filebrowserImageBrowseUrl : '<?php echo base_url(); ?>assets/kcfinder'
} );

$('.daterange-single').daterangepicker({ 
	singleDatePicker: true,
	locale: {
      format: 'DD/MM/YYYY'
    }
});
</script>