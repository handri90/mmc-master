<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('berita_model');
	}

	function get_data_berita(){
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");
        $con_where = "";
        if($this->session->userdata('level_user_id') == '3'){
            $con_where = array(
                "user_id_created"=>$this->session->userdata('id_user')
            );
        }

        $data_berita = $this->berita_model->get(
            array(
                "fields"=>"berita.*,user.nama_lengkap,DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') AS tanggal_publish_show",
                "join"=>array(
                    "user"=>"user_id_created=id_user"
                ),
                "where"=>$con_where,
                "where_false"=>"DATE_FORMAT(ifnull(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '".$startDate."' AND '".$endDate."'",
                "order_by"=>array(
                    "tanggal_publish_show"=>"DESC"
                )
            )
        );

        $templist = array();
        foreach($data_berita as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_berita);
            $templist[$key]['tanggal_kegiatan_custom'] = longdate_indo($row->tanggal_publish_show);
            $judul=strip_tags(html_entity_decode($row->judul));
            $judul= preg_replace('/\s+?(\S+)?$/', '', substr($judul, 0, 50));
            $templist[$key]['judul_custom'] = $judul."...";
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_news(){
        $id_berita = decrypt_data($this->iget("id_berita"));

        $data = $this->berita_model->get(
            array(
                "fields"=>"berita.*,DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') as tanggal_publish",
                "where"=>array(
                    "id_berita"=>$id_berita
                )
            ),"row"
        );

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
