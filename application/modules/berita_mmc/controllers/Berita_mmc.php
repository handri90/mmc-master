<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_mmc extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('berita_model');
        $this->load->model('kategori_berita/kategori_berita_model','kategori_berita_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Berita','is_active'=>true]];

        if($this->session->userdata('level_user_id') != '3'){
            $this->execute('index',$data);
        }else{
            $this->execute('index_kontributor',$data);
        }
    }
    
    public function tambah_berita(){
        if(empty($_POST)){
            $data['kategori_berita'] = $this->kategori_berita_model->get(
                array(
                    "order_by"=>array(
                        "nama_kategori_berita"=>"ASC"
                    )
                )
            );

            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'berita_mmc','content'=>'Berita','is_active'=>false],['link'=>false,'content'=>'Tambah Berita','is_active'=>true]];
            $this->execute('form_berita',$data);
        }else{
            $input_name = 'foto_berita';
            $upload_foto = $this->upload_file($input_name,$this->config->item('berita_path'),'berita');

            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->ipost('judul'))));
            $cek_slug = $this->berita_model->get(
                array(
                    "fields"=>"IFNULL(count(*),0) as jumlah_slug",
                    "where"=>array(
                        "slug_berita"=>$slug
                    )
                ),"row"
            );

            if($cek_slug->jumlah_slug != '0'){
                $slug .= $cek_slug->jumlah_slug;
            }

            $date_exp = explode("/",$this->ipost('set_tanggal_publish'));
            $set_publish = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];
            
            if(!isset($upload_foto['error'])){
                $data = array(
                    "judul"=>$this->ipost('judul'),
                    "deskripsi"=>htmlentities($this->ipost('content')),
                    "kategori_berita_id"=>decrypt_data($this->ipost('id_kategori')),
                    "set_tanggal_publish"=>date("Y-m-d H:i:s",strtotime($set_publish)),
                    "slug_berita"=>$slug,
                    "filename"=>$upload_foto['data']['file_name'],
                    "caption"=>$this->ipost('caption'),
                    "user_id_created"=>$this->session->userdata('id_user'),
                    'created_at'=>$this->datetime()
                );
    
                $status = $this->berita_model->save($data);
                if($status){
                    $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
                }else{
                    $this->session->set_flashdata('message','Data baru gagal ditambahkan');
                }
                
            }else{
                $this->session->set_flashdata('message','Gagal upload foto');
            }

            redirect('berita_mmc');
        }
    }

    public function edit_berita($id_berita){
        $data_master = $this->berita_model->get(
            array(
                "fields"=>"berita.id_berita,berita.is_active,berita.judul,berita.slug_berita,berita.deskripsi,berita.kategori_berita_id,DATE_FORMAT(berita.set_tanggal_publish,'%d/%m/%Y') AS set_tanggal_publish,berita.filename,berita.caption",
                "where"=>array(
                    "id_berita"=>decrypt_data($id_berita)
                )
            ),"row"
        );

        if(!$data_master){
            $this->page_error();
        }

        if($this->session->userdata('level_user_id') == '3'){
            if($data_master->is_active == '1'){
                $this->session->set_flashdata('message','Maaf, berita sudah di publish.');
                redirect('berita_mmc');
            }
        }

        if(empty($_POST)){
            $data['kategori_berita'] = $this->kategori_berita_model->get(
                array(
                    "order_by"=>array(
                        "nama_kategori_berita"=>"ASC"
                    )
                )
            );
            
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'berita_mmc','content'=>'Berita','is_active'=>false],['link'=>false,'content'=>'Tambah Berita','is_active'=>true]];
            $this->execute('form_berita',$data);
        }else{

            if($this->ipost('judul') != $data_master->judul){
                $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->ipost('judul'))));
                $cek_slug = $this->berita_model->get(
                    array(
                        "fields"=>"IFNULL(count(*),0) as jumlah_slug",
                        "where"=>array(
                            "slug_berita"=>$slug
                        ),
                        "where_false"=>"id_berita != ".decrypt_data($id_berita)
                    ),"row"
                );
    
                if($cek_slug->jumlah_slug != '0'){
                    $slug .= $cek_slug->jumlah_slug;
                }
            }else{
                $slug = $data_master->slug_berita;
            }

            $filename = "";
            if($_FILES['foto_berita']['name'] != ""){
                if(is_file($this->config->item('berita_path')."/".$data_master->filename)){
                    unlink($this->config->item('berita_path')."/".$data_master->filename);
                }
                
                if(is_file($this->config->item('berita_path')."/thumbnail/".$data_master->filename)){
                    unlink($this->config->item('berita_path')."/thumbnail/".$data_master->filename);
                }
                $input_name = 'foto_berita';
                $upload_foto = $this->upload_file($input_name,$this->config->item('berita_path'),'berita');
                $filename = $upload_foto['data']['file_name'];
            }else{
                $filename = $data_master->filename;
            }

            $date_exp = explode("/",$this->ipost('set_tanggal_publish'));
            $set_publish = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];
            
            $data = array(
                "judul"=>$this->ipost('judul'),
                "deskripsi"=>htmlentities($this->ipost('content')),
                "kategori_berita_id"=>decrypt_data($this->ipost('id_kategori')),
                "set_tanggal_publish"=>date("Y-m-d H:i:s",strtotime($set_publish)),
                "slug_berita"=>$slug,
                "filename"=>$filename,
                "caption"=>$this->ipost('caption'),
                "user_id_updated"=>$this->session->userdata('id_user'),
                'updated_at'=>$this->datetime()
            );

            $status = $this->berita_model->edit(decrypt_data($id_berita),$data);
            if($status){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('berita_mmc');
        }
    }

    public function delete_berita(){
        $id_berita = $this->iget('id_berita');
        $data_master = $this->berita_model->get_by(decrypt_data($id_berita));

        if(!$data_master){
            $this->page_error();
        }

        if($this->session->userdata('level_user_id') == '3'){
            if($data_master->is_active == '1'){
                $status = false;
            }else{
                if(is_file($this->config->item('berita_path')."/".$data_master->filename)){
                    unlink($this->config->item('berita_path')."/".$data_master->filename);
                }
                
                if(is_file($this->config->item('berita_path')."/thumbnail/".$data_master->filename)){
                    unlink($this->config->item('berita_path')."/thumbnail/".$data_master->filename);
                }

                $status = $this->berita_model->remove(decrypt_data($id_berita));
            }
        }else{

            if(is_file($this->config->item('berita_path')."/".$data_master->filename)){
                unlink($this->config->item('berita_path')."/".$data_master->filename);
            }
            
            if(is_file($this->config->item('berita_path')."/thumbnail/".$data_master->filename)){
                unlink($this->config->item('berita_path')."/thumbnail/".$data_master->filename);
            }
            
            $status = $this->berita_model->remove(decrypt_data($id_berita));
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function change_status(){
        $id_berita = $this->iget('id_berita');
        $data_master = $this->berita_model->get(
            array(
                "fields"=>"berita.*,DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') as tanggal_publish",
                "where"=>array(
                    "id_berita"=>decrypt_data($id_berita)
                )
            ),"row"
        );

        if(!$data_master){
            $this->page_error();
        }

        if($data_master->is_active == '1'){
            $change_data = array(
                "is_active"=>"0"
            );
        }else{
            $change_data = array(
                "is_active"=>"1"
            );
        }

        $status = $this->berita_model->edit(decrypt_data($id_berita),$change_data);

        if($status){
            $status = $data_master;
        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
