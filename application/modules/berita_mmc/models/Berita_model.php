<?php

class Berita_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="berita";
        $this->primary_id="id_berita";
    }
}