<?php

class Gallery_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="gallery";
        $this->primary_id="id_gallery";
    }
}