<?php

class Gallery_detail_model extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->table="gallery_detail";
        $this->primary_id="id_gallery_detail";
    }
}