<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url().'gallery/tambah_gallery'; ?>" class="btn btn-info">Tambah Gallery</a>
            </div>
        </div>
        <table id="datatableGallery" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Gallery</th>
                    <th>Tanggal Kegiatan</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
let datatableGallery = $("#datatableGallery").DataTable({
    "columns":[
        {"width":"46"},
        null,
        {"width":"196"},
        {"width":"46"},
        {"width":"166"}
    ]
});
get_data_gallery();
function get_data_gallery(){
    datatableGallery.clear().draw();
    $.ajax({
            url: base_url+'gallery/request/get_data_gallery',
            type: 'GET',
            beforeSend: function(){
                loading_start();
            },
            success: function(response){
                let no = 1;
                $.each(response,function(index,value){
                    datatableGallery.row.add([
                        no,
                        value.nama_gallery,
                        value.tanggal_kegiatan_custom,
                        (value.is_active=='1'?'<span class="badge badge-success">Publish</span>':'<span class="badge badge-warning">Pending</span>'),
                        "<a href='"+base_url+"gallery/edit_gallery/"+value.id_encrypt+"' class='btn btn-primary btn-icon btn-sm'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon btn-sm' onClick=\"confirm_delete('"+value.id_encrypt+"')\" href='#'><i class='icon-trash'></i></a> <a class='btn btn-info btn-icon btn-sm' onClick=\"change_status('"+value.id_encrypt+"')\" href='#'><i class='icon-cloud'></i></a>"
                    ]).draw(false);
                    no++;
                });
            },
            complete:function(){
                loading_stop();
            }
        });
}

function confirm_delete(id_gallery){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'gallery/delete_gallery',
                data : {id_gallery:id_gallery},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_gallery();
                        swalInit(
                            'Berhasil',
                            'Data sudah dihapus',
                            'success'
                        );
                    }else{
                        get_data_gallery();
                        swalInit(
                            'Gagal',
                            'Data tidak bisa dihapus',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_gallery();
                }
            });
        }
    });
}

function change_status(id_gallery){
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    swalInit({
        title: 'Apakah anda yakin ingin merubah status gallery ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: base_url+'gallery/change_status',
                data : {id_gallery:id_gallery},
                type: 'GET',
                beforeSend: function(){
                    loading_start();
                },
                success: function(response){
                    if(response){
                        get_data_gallery();
                        swalInit(
                            'Berhasil',
                            'Status gallery berhasil diubah',
                            'success'
                        );
                    }else{
                        get_data_gallery();
                        swalInit(
                            'Gagal',
                            'Tidak bisa merubah status gallery',
                            'error'
                        );
                    }
                },
                complete:function(response){
                    loading_stop();
                }
            });
        }
        else if(result.dismiss === swal.DismissReason.cancel) {
            swalInit(
                'Batal',
                'Data masih tersimpan!',
                'error'
            ).then(function(results){
                loading_stop();
                if(result.results){
                    get_data_gallery();
                }
            });
        }
    });
}
</script>