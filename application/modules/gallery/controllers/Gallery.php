<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('gallery_model');
		$this->load->model('gallery_detail_model');
	}

	public function index()
	{
        $data['breadcrumb'] = [['link'=>false,'content'=>'Gallery','is_active'=>true]];
        $this->execute('index',$data);
    }
    
    public function tambah_gallery(){
        if(empty($_FILES)){
            $data["kategori_gallery"] = array(
                "1"=>"Kegiatan",
                "2"=>"Tempo Dulu",
            );
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'gallery','content'=>'Gallery','is_active'=>false],['link'=>false,'content'=>'Tambah Gallery','is_active'=>true]];
            $this->execute('form_gallery',$data);
        }else{
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->ipost('nama_gallery'))));
            $cek_slug = $this->gallery_model->get(
                array(
                    "fields"=>"IFNULL(count(*),0) as jumlah_slug",
                    "where"=>array(
                        "slug_gallery"=>$slug
                    )
                ),"row"
            );

            if($cek_slug->jumlah_slug != '0'){
                $slug .= $cek_slug->jumlah_slug;
            }

            $date_exp = explode("/",$this->ipost('tanggal_kegiatan'));
            $tanggal_kegiatan = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];

            $data = array(
                "nama_gallery"=>$this->ipost('nama_gallery'),
                "tanggal_kegiatan"=>date("Y-m-d H:i:s",strtotime($tanggal_kegiatan)),
                "slug_gallery"=>$slug,
                "kategori_foto"=>$this->ipost('id_kategori'),
                "is_active"=>"0",
                "user_id_created"=>$this->session->userdata('id_user'),
                'created_at'=>$this->datetime()
            );

            $id_gallery = $this->gallery_model->save($data);

            $upload_path = $this->config->item('gallery_path');
            $name_field = 'foto_gallery';
            $status = "";

            if((!file_exists($upload_path)) && !(is_dir($upload_path))){
                mkdir($upload_path);
            }
            $config = array();
            // $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
    
            $count_files = count($_FILES[$name_field]['name']);
            $files = $_FILES;
    
            for($i = 0;$i < $count_files; $i++){
                if($files[$name_field]['name'][$i]){
                    $_FILES[$name_field]['name']= $files[$name_field]['name'][$i];
                    $_FILES[$name_field]['type']= $files[$name_field]['type'][$i];
                    $_FILES[$name_field]['tmp_name']= $files[$name_field]['tmp_name'][$i];
                    $_FILES[$name_field]['error']= $files[$name_field]['error'][$i];
                    $_FILES[$name_field]['size']= $files[$name_field]['size'][$i];


                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload($name_field))
                    {
                        $status = array('error' => $this->upload->display_errors());
                    }
                    else
                    {
                        $status = array('data' => $this->upload->data());
                    }
                
                    $filename = $status['data']['file_name'];
                    $tanggal_upload = date('Y-m-d H:i:s');

                    $data_gallery_detail = array(
                        "filename"=>$filename,
                        "gallery_id"=>$id_gallery,
                        "img_size"=>$_FILES[$name_field]['size'],
                        "created_at"=>$this->datetime()
                    );

                    $this->gallery_detail_model->save($data_gallery_detail);
                }

            }
            
            if($id_gallery){
                $this->session->set_flashdata('message','Data baru berhasil ditambahkan');
            }else{
                $this->session->set_flashdata('message','Data baru gagal ditambahkan');
            }

            redirect('gallery/tambah_gallery');
        }
    }

    public function edit_gallery($id_gallery){
        $data_master = $this->gallery_model->get(
            array(
                "fields"=>"gallery.*,group_concat(gallery_detail.id_gallery_detail separator '|') as id_gallery_detail,group_concat(gallery_detail.filename separator '|') as filename,DATE_FORMAT(gallery.tanggal_kegiatan,'%d/%m/%Y') AS tanggal_kegiatan",
                "join"=>array(
                    "gallery_detail"=>"id_gallery=gallery_id AND gallery_detail.deleted_at IS NULL"
                ),
                "where"=>array(
                    "id_gallery"=>decrypt_data($id_gallery)
                ),
                "group_by"=>"id_gallery"
            ),"row"
        );

        if(!$data_master){
            $this->page_error();
        }

        if(empty($_POST)){
            $data["kategori_gallery"] = array(
                "1"=>"Kegiatan",
                "2"=>"Tempo Dulu",
            );
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link'=>true,'url'=>base_url().'gallery','content'=>'Gallery','is_active'=>false],['link'=>false,'content'=>'Tambah Gallery','is_active'=>true]];
            $this->execute('form_gallery',$data);
        }else{
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->ipost('nama_gallery'))));
            $cek_slug = $this->gallery_model->get(
                array(
                    "fields"=>"IFNULL(count(*),0) as jumlah_slug",
                    "where"=>array(
                        "slug_gallery"=>$slug
                    ),
                    "where_false"=>"id_gallery != ".decrypt_data($id_gallery)
                ),"row"
            );

            if($cek_slug->jumlah_slug != '0'){
                $slug .= $cek_slug->jumlah_slug;
            }

            $date_exp = explode("/",$this->ipost('tanggal_kegiatan'));
            $set_tanggal = $date_exp[2]."-".$date_exp[1]."-".$date_exp[0];

            $data = array(
                "nama_gallery"=>$this->ipost('nama_gallery'),
                "tanggal_kegiatan"=>date("Y-m-d H:i:s",strtotime($set_tanggal)),
                "slug_gallery"=>$slug,
                "kategori_foto"=>$this->ipost('id_kategori'),
                "user_id_updated"=>$this->session->userdata('id_user'),
                'updated_at'=>$this->datetime()
            );

            $status_id_gallery = $this->gallery_model->edit(decrypt_data($id_gallery),$data);

            $upload_path = $this->config->item('gallery_path');
            $name_field = 'foto_gallery';
            $status = "";

            if((!file_exists($upload_path)) && !(is_dir($upload_path))){
                mkdir($upload_path);
            }
            $config = array();
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = $filename;
    
            $count_files = count($_FILES[$name_field]['name']);
            $files = $_FILES;

            if($count_files != 0){
                for($i = 0;$i < $count_files; $i++){
                    if($files[$name_field]['name'][$i]){
                        $_FILES[$name_field]['name']= $files[$name_field]['name'][$i];
                        $_FILES[$name_field]['type']= $files[$name_field]['type'][$i];
                        $_FILES[$name_field]['tmp_name']= $files[$name_field]['tmp_name'][$i];
                        $_FILES[$name_field]['error']= $files[$name_field]['error'][$i];
                        $_FILES[$name_field]['size']= $files[$name_field]['size'][$i];
    
                        $this->load->library('upload', $config);
    
                        if ( ! $this->upload->do_upload($name_field))
                        {
                            $status = array('error' => $this->upload->display_errors());
                        }
                        else
                        {
                            $status = array('data' => $this->upload->data());
                        }
                    
                        $filename = $status['data']['file_name'];
                        $tanggal_upload = date('Y-m-d H:i:s');
    
                        $data_gallery_detail = array(
                            "filename"=>$filename,
                            "gallery_id"=>decrypt_data($id_gallery),
                            "img_size"=>$_FILES[$name_field]['size'],
                            "created_at"=>$this->datetime()
                        );

                        $this->gallery_detail_model->save($data_gallery_detail);
                    }
                }
            }
            if($status_id_gallery){
                $this->session->set_flashdata('message','Data berhasil diubah');
            }else{
                $this->session->set_flashdata('message','Data gagal diubah');
            }

            redirect('gallery');
        }
    }

    public function thumbnailResizeImage($filename,$filepath)
    {
        $source_path = $filepath. '/' . $filename;
        $target_path = $filepath;

        if((!file_exists($target_path."/thumbnail")) && !(is_dir($target_path."/thumbnail"))){
            mkdir($target_path."/thumbnail");
        }
        
        $config = array(
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path."/thumbnail/".$filename,
                'maintain_ratio' => TRUE,
                // 'quality' => '100',
                'width' => 250  
            ),
            array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path."/".$filename,
                'maintain_ratio' => TRUE,
                // 'quality' => '100',
                'width' => 700  
            )
        );

        $this->load->library('image_lib', $config[0]);
        foreach($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

    public function delete_gallery(){
        $id_gallery = $this->iget('id_gallery');
        $data_master = $this->gallery_model->get_by(decrypt_data($id_gallery));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->gallery_model->remove(decrypt_data($id_gallery));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
    
    public function delete_gallery_detail(){
        $id_gallery_detail = $this->iget('id_gallery_detail');
        $data_master = $this->gallery_detail_model->get_by(decrypt_data($id_gallery_detail));

        if(!$data_master){
            $this->page_error();
        }

        $status = $this->gallery_detail_model->remove(decrypt_data($id_gallery_detail));
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }

    public function change_status(){
        $id_gallery = $this->iget('id_gallery');
        $data_master = $this->gallery_model->get_by(decrypt_data($id_gallery));
        if(!$data_master){
            $this->page_error();
        }

        if($data_master->is_active == '1'){
            $change_data = array(
                "is_active"=>"0"
            );
        }else{
            $change_data = array(
                "is_active"=>"1"
            );
        }

        $status = $this->gallery_model->edit(decrypt_data($id_gallery),$change_data);
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($status));
    }
}
