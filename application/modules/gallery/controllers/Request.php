<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('gallery_model');
	}

	function get_data_gallery(){
        $data_gallery = $this->gallery_model->get(
            array(
                "fields"=>"gallery.*,DATE_FORMAT(IFNULL(tanggal_kegiatan,created_at), '%Y-%m-%d') AS tanggal_kegiatan",
                "order_by"=>array(
                    "tanggal_kegiatan"=>"DESC"
                )
            )
        );

        $templist = array();
        foreach($data_gallery as $key=>$row){
            foreach($row as $keys=>$rows){
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_gallery);
            $templist[$key]['tanggal_kegiatan_custom'] = longdate_indo($row->tanggal_kegiatan);
        }

        $data = $templist;
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
}
