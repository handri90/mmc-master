	<!-- Content area -->
	<div class="content">
		<div class="card border-top-success">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Tanggal : </label>
							<button type="button" class="btn btn-light daterange-predefined">
								<i class="icon-calendar22 mr-2"></i>
								<span></span>
								<input type="hidden" name="startDate" />
								<input type="hidden" name="endDate" />
							</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Kategori Berita : </label>
							<select class="form-control select-search" name="kategori_berita" onChange="spin_kategori()">
								<option value="">-- SEMUA --</option>
								<?php
								foreach ($list_kategori_berita as $key => $row) {
								?>
									<option value="<?php echo encrypt_data($row->id_kategori_berita); ?>"><?php echo $row->nama_kategori_berita; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card card-table">
			<div class="card-header header-elements-inline">
				<h6 class="card-title">Jumlah Berita Per SKPD ( <strong><span class="show_tanggal"></span></strong> )</h6>
			</div>
			<div class="card-body">
				<div id="google-bar"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-7">
				<div class="card border-top-success">
					<div class="card-header header-elements-inline">
						<h6 class="card-title">Statistik Kontributor ( <strong><span class="show_tanggal"></span></strong> )</h6>
					</div>
					<div class="card-body">
						<table class="table datatable-save-state table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Kontributor</th>
									<th>Berita Publish</th>
									<th>Jumlah Pembaca</th>
									<th>Rata-rata (Pembaca/berita)</th>
									<th>Kontribusi</th>
								</tr>
							</thead>
							<tbody class="tbody-statisktik-kontributor">
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-xl-5">
				<div class="card border-top-success">
					<div class="card-header header-elements-inline">
						<h6 class="card-title"> Top 5 (Kontributor Berita Terbanyak) ( <strong><span class="show_tanggal"></span></strong> )</h6>
					</div>
					<div class="card-body">
						<table class="table datatable-save-state table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Kontributor</th>
									<th>Berita Publish</th>
								</tr>
							</thead>
							<tbody class="tbody-kontributor-berita-terbanyak">
							</tbody>
						</table>
					</div>
				</div>
				<div class="card border-top-success">
					<div class="card-header header-elements-inline">
						<h6 class="card-title"> Top 5 (Jumlah Pembaca Terbanyak) ( <strong><span class="show_tanggal"></span></strong> )</h6>
					</div>
					<div class="card-body">
						<table class="table datatable-save-state table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Kontributor</th>
									<th>Jumlah Pembaca</th>
								</tr>
							</thead>
							<tbody class="tbody-kontributor-pembaca-terbanyak">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /content area -->


	<script>
		function spin_kategori() {
			draw_bar();
			get_statistik_kontributor();
			top_kontributor_berita_terbanyak();
			top_kontributor_pembaca_terbanyak();
		}

		function get_statistik_kontributor() {
			let startDate = $("input[name=startDate]").val();
			let endDate = $("input[name=endDate]").val();
			let kategori_berita = $("select[name=kategori_berita]").val();

			$(".tbody-statisktik-kontributor").html("<tr><td colspan='6' style='text-align:center;'>No data available in table</td></tr>");
			$.ajax({
				url: base_url + 'dashboard/request/get_statistik_kontributor',
				data: {
					startDate: startDate,
					endDate: endDate,
					kategori_berita: kategori_berita
				},
				type: 'GET',
				beforeSend: function() {
					loading_start();
				},
				success: function(response) {
					$(".tbody-statisktik-kontributor").html("<tr><td colspan='6' style='text-align:center;'>No data available in table</td></tr>");
					let no = 1;
					let html = "";
					$.each(response, function(index, value) {
						html += "<tr><td>" + no + "</td><td>" + value.nama_lengkap + "</td><td>" + value.jumlah_berita + "</td><td>" + value.jumlah_pembaca + "</td><td>" + value.rata_rata_pembaca + "</td><td>" + value.persentase + "%</td></tr>";
						no++;
					});
					$(".tbody-statisktik-kontributor").html(html);

				},
				complete: function() {
					loading_stop();
				}
			});
		}

		function top_kontributor_berita_terbanyak() {
			let startDate = $("input[name=startDate]").val();
			let endDate = $("input[name=endDate]").val();
			let kategori_berita = $("select[name=kategori_berita]").val();

			$(".tbody-kontributor-berita-terbanyak").html("<tr><td colspan='3' style='text-align:center;'>No data available in table</td></tr>");
			$.ajax({
				url: base_url + 'dashboard/request/get_kontributor_berita_terbanyak',
				data: {
					startDate: startDate,
					endDate: endDate,
					kategori_berita: kategori_berita
				},
				type: 'GET',
				beforeSend: function() {
					loading_start();
				},
				success: function(response) {
					$(".tbody-kontributor-berita-terbanyak").html("<tr><td colspan='3' style='text-align:center;'>No data available in table</td></tr>");
					let no = 1;
					let html = "";
					$.each(response, function(index, value) {
						html += "<tr><td>" + no + "</td><td>" + value.nama_lengkap + "</td><td>" + value.jumlah_berita + "</td></tr>";
						no++;
					});
					$(".tbody-kontributor-berita-terbanyak").html(html);

				},
				complete: function() {
					loading_stop();
				}
			});
		}

		function top_kontributor_pembaca_terbanyak() {
			let startDate = $("input[name=startDate]").val();
			let endDate = $("input[name=endDate]").val();
			let kategori_berita = $("select[name=kategori_berita]").val();

			$(".tbody-kontributor-pembaca-terbanyak").html("<tr><td colspan='3' style='text-align:center;'>No data available in table</td></tr>");
			$.ajax({
				url: base_url + 'dashboard/request/get_kontributor_pembaca_terbanyak',
				data: {
					startDate: startDate,
					endDate: endDate,
					kategori_berita: kategori_berita
				},
				type: 'GET',
				beforeSend: function() {
					loading_start();
				},
				success: function(response) {
					$(".tbody-kontributor-pembaca-terbanyak").html("<tr><td colspan='3' style='text-align:center;'>No data available in table</td></tr>");
					let no = 1;
					let html = "";
					$.each(response, function(index, value) {
						html += "<tr><td>" + no + "</td><td>" + value.nama_lengkap + "</td><td>" + value.jumlah_pembaca + "</td></tr>";
						no++;
					});
					$(".tbody-kontributor-pembaca-terbanyak").html(html);

				},
				complete: function() {
					loading_stop();
				}
			});
		}

		function draw_bar() {
			// Initialize chart
			google.charts.load('current', {
				callback: function() {

					// Draw chart
					// Define charts element
					var bar_chart_element = document.getElementById('google-bar');

					let startDate = $("input[name=startDate]").val();
					let endDate = $("input[name=endDate]").val();
					let kategori_berita = $("select[name=kategori_berita]").val();

					$.ajax({
						url: base_url + 'dashboard/request/get_laporan_1',
						data: {
							startDate: startDate,
							endDate: endDate,
							kategori_berita: kategori_berita
						},
						type: 'GET',
						beforeSend: function() {
							loading_start();
						},
						success: function(response) {
							// Data
							var data = google.visualization.arrayToDataTable(response);


							// Options
							var options_bar = {
								fontName: 'Roboto',
								height: 900,
								fontSize: 10,
								backgroundColor: 'transparent',
								chartArea: {
									left: '33%',
									width: '95%',
									height: 850
								},
								tooltip: {
									textStyle: {
										fontName: 'Roboto',
										fontSize: 13
									}
								},
								vAxis: {
									textStyle: {
										color: '#333'
									},
									gridlines: {
										count: 10
									},
									minValue: 0
								},
								hAxis: {
									baselineColor: '#ccc',
									textStyle: {
										color: '#333'
									},
									gridlines: {
										color: '#eee'
									}
								},
								legend: {
									position: 'top',
									alignment: 'center',
									textStyle: {
										color: '#333'
									}
								},
								series: {
									0: {
										color: '#ffb980'
									},
									1: {
										color: '#66BB6A'
									}
								}
							};

							// Draw chart
							var bar = new google.visualization.BarChart(bar_chart_element);
							bar.draw(data, options_bar);
						},
						complete: function() {
							loading_stop();
						}
					});

					// Resize on sidebar width change
					var sidebarToggle = document.querySelector('.sidebar-control');
					sidebarToggle && sidebarToggle.addEventListener('click', draw_bar);

					// Resize on window resize
					var resizeBarBasic;
					window.addEventListener('resize', function() {
						clearTimeout(resizeBarBasic);
						resizeBarBasic = setTimeout(function() {
							draw_bar();
						}, 200);
					});
				},
				packages: ['corechart']
			});
		}


		$('.daterange-predefined').daterangepicker({
				startDate: moment().subtract(29, 'days'),
				endDate: moment(),
				minDate: '01/01/2018',
				maxDate: '12/31/2022',
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				},
				opens: 'right',
				applyClass: 'btn-sm btn-primary',
				cancelClass: 'btn-sm btn-light'
			},
			function(start, end) {
				$('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
				$("input[name=startDate]").val(start.format('YYYY-MM-DD'));
				$("input[name=endDate]").val(end.format('YYYY-MM-DD'));
				$(".show_tanggal").html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
				draw_bar();
				get_statistik_kontributor();
				top_kontributor_berita_terbanyak();
				top_kontributor_pembaca_terbanyak();
			}
		);

		// Display date format
		$('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
		$("input[name=startDate]").val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
		$("input[name=endDate]").val(moment().format('YYYY-MM-DD'));
		$(".show_tanggal").html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
		draw_bar();
		get_statistik_kontributor();
		top_kontributor_berita_terbanyak();
		top_kontributor_pembaca_terbanyak();
	</script>