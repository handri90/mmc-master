<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("kategori_berita/kategori_berita_model", "kategori_berita_model");
    }

    public function index()
    {
        $data['list_kategori_berita'] = $this->kategori_berita_model->get(
            array(
                "order_by" => array(
                    "nama_kategori_berita" => "ASC"
                )
            )
        );
        $data['breadcrumb'] = [];
        $this->execute('index', $data);
    }
}
