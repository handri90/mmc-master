<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
        $this->load->model('berita_mmc/berita_model', 'berita_model');
    }

    function get_laporan_1()
    {
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");
        $kategori_berita = decrypt_data($this->iget("kategori_berita"));

        $wh = "";
        if ($kategori_berita) {
            $wh = "AND kategori_berita_id = " . $kategori_berita;
        }

        $data_laporan_1 = $this->master_satker_model->query("
        SELECT master_satker.nama,jumlah_berita
        FROM master_satker
        INNER JOIN (
            SELECT COUNT(id_berita) AS jumlah_berita,master_satker_id
            FROM user
            LEFT JOIN berita ON user_id_created=id_user " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND is_active = '1'
            WHERE user.deleted_at IS NULL
            GROUP BY master_satker_id
        ) AS a ON a.master_satker_id=id_master_satker
        ORDER BY jumlah_berita DESC
        ")->result();

        $templist = array();

        array_push($templist, array('SKPD', 'Jumlah Berita'));

        foreach ($data_laporan_1 as $key => $row) {
            array_push($templist, array($row->nama, (int) $row->jumlah_berita));
        }

        $data = $templist;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_statistik_kontributor()
    {
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");

        $kategori_berita = decrypt_data($this->iget("kategori_berita"));

        $wh = "";
        if ($kategori_berita) {
            $wh = "AND kategori_berita_id = " . $kategori_berita;
        }

        $data = $this->berita_model->query("
        SELECT id_user,nama_lengkap,IFNULL(jumlah_berita,0) AS jumlah_berita,IFNULL(jumlah_pembaca,0) AS jumlah_pembaca,IFNULL(ROUND(jumlah_pembaca/jumlah_berita,2),0) AS rata_rata_pembaca,
        IFNULL(
        ROUND(((jumlah_berita)/(SELECT COUNT(id_berita) jumlah_berita
        FROM berita 
        WHERE is_active = '1' 
        " . $wh . " 
        AND berita.`deleted_at` IS NULL 
        AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'))*100,2),0
        ) AS persentase
        FROM user
        LEFT JOIN (
            SELECT COUNT(id_berita) jumlah_berita,user_id_created,SUM(read_count) AS jumlah_pembaca
            FROM berita 
            WHERE is_active = '1' " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'
            GROUP BY user_id_created
            )AS a ON a.user_id_created=id_user
        ORDER BY jumlah_berita DESC
        ")->result();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_kontributor_berita_terbanyak()
    {
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");
        $kategori_berita = decrypt_data($this->iget("kategori_berita"));

        $wh = "";
        if ($kategori_berita) {
            $wh = "AND kategori_berita_id = " . $kategori_berita;
        }

        $data = $this->berita_model->query("
        SELECT id_user,nama_lengkap,IFNULL(jumlah_berita,0) AS jumlah_berita
        FROM user
        LEFT JOIN (
            SELECT COUNT(id_berita) jumlah_berita,user_id_created,SUM(read_count) AS jumlah_pembaca
            FROM berita 
            WHERE is_active = '1' " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'
            GROUP BY user_id_created
            )AS a ON a.user_id_created=id_user
        ORDER BY jumlah_berita DESC
        LIMIT 5
        ")->result();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_kontributor_pembaca_terbanyak()
    {
        $startDate = $this->iget("startDate");
        $endDate = $this->iget("endDate");
        $kategori_berita = decrypt_data($this->iget("kategori_berita"));

        $wh = "";
        if ($kategori_berita) {
            $wh = "AND kategori_berita_id = " . $kategori_berita;
        }

        $data = $this->berita_model->query("
        SELECT id_user,nama_lengkap,IFNULL(jumlah_pembaca,0) AS jumlah_pembaca
        FROM user
        LEFT JOIN (
            SELECT COUNT(id_berita) jumlah_berita,user_id_created,SUM(read_count) AS jumlah_pembaca
            FROM berita 
            WHERE is_active = '1' " . $wh . " AND berita.`deleted_at` IS NULL AND DATE_FORMAT(IFNULL(set_tanggal_publish,berita.created_at), '%Y-%m-%d') BETWEEN '" . $startDate . "' AND '" . $endDate . "'
            GROUP BY user_id_created
            )AS a ON a.user_id_created=id_user
        ORDER BY jumlah_pembaca DESC
        LIMIT 5
        ")->result();

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
